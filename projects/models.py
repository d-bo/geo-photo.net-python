from django.db import models


""" Shared projects """
class shared(models.Model):
    shared_project_id = models.IntegerField(blank=False, null=True)
    shared_project_name = models.TextField(max_length=2000, blank=True, null=True)
    shared_user_id = models.IntegerField(blank=False, null=True)
