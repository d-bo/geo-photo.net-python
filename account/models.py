from django.db import models

""" Saved user sessions (additional data) """
class sessions(models.Model):
    session_user_id = models.IntegerField(blank=True, null=True)
    session_data = models.CharField(max_length=5000, blank=True, null=True)
    session_hash = models.CharField(max_length=5000, blank=True, null=True)
