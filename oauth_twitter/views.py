# -*- coding: utf-8 -*-

from cgi import escape
import cgi
import cgitb; cgitb.enable()
import sys, os
import Cookie
import tweepy
import urllib
import hashlib

from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.models import User

from mobile.views import isApple
from mobile.views import isAndroid
from account.views import create_account
from account.views import login_user

from account.models import sessions


TWITTER_CONSUMER_KEY = '189E1P4NrEuzxXpFKYA'
TWITTER_CONSUMER_SECRET = 'EQScD3Dq4oKzHvkJwmmFXY4UJgnsusplyUnRAxWAU'


def index(request):
    
    """ Index redirect """
    
    a_cookie = Cookie.SimpleCookie()
    
    auth = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
    
    try:
        redirect_url = auth.get_authorization_url()
    except tweepy.TweepError:
        print 'Error! Failed to get request token.'
    
    a_cookie['request_token_key'] = auth.request_token.key
    a_cookie['request_token_secret'] = auth.request_token.secret
    
    response = redirect(redirect_url)
    response.set_cookie('request_token_key', auth.request_token.key)
    response.set_cookie('request_token_secret', auth.request_token.secret)
    
    return response


def callback(request):
    
    """ Handle oauth callback url """
    
    request_token_key = request.COOKIES["request_token_key"]
    request_token_secret = request.COOKIES["request_token_secret"]
    
    verifier = request.GET["oauth_verifier"]
    
    auth = tweepy.OAuthHandler(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET)
    auth.set_request_token(request_token_key, request_token_secret)
    
    try:
        auth.get_access_token(verifier)
    except tweepy.TweepError:
        print 'Error! Failed to get access token.'
    
    auth.set_access_token(auth.access_token.key, auth.access_token.secret)
    
    api = tweepy.API(auth)
    test=api.me()
    
    userdata = {}
    
    userdata['name'] = test.name.encode('utf8')
    
    # twitter api doesn't support email response
    userdata['email'] = ''
    userdata['pwd'] = ''
    
    try:
        user = User.objects.get(username=userdata['name'])
    except:
        user = create_account(userdata['name'], userdata['email'])
        if user == False:
            return HttpResponse('Cannot create user. Contact site administrator.')
        namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
    else:
        namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
        try:
            ss = sessions.objects.get(session_user_id=user.id)
        except:
            out = {}
            #namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
            ss = sessions(session_user_id=user.id,session_data=out,session_hash=namehash)
            ss.save()
        else:
            pass
        
        a = HttpResponse('<script type="text/javascript">window.opener.location.reload(false);window.close()</script>')
        
        #a = HttpResponse(namehash)
        
        if isApple(request) != False:
            request = urllib2.Request('geocam://_trace=' + str(namehash) + ';name= ' + str(userdata['name']) + ';status=200')
            return a
        if isAndroid(request) != False:
            a.set_cookie('_trace', namehash)
            a.set_cookie('status', '200')
            return a
        
        login_user(request, user)
        return a
        
    #return HttpResponse(test.name)
