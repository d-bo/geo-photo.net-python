from django.db import models

class filters(models.Model):
    filter_id = models.AutoField(primary_key=True)
    filter_name = models.CharField(max_length=1000, blank=True, null=True)
    filter_user_id = models.IntegerField(blank=True, null=True)
    filter_project_id = models.IntegerField(blank=True, null=True)
    filter_create_date = models.CharField(max_length=1000,blank=True, null=True)
    filter_data = models.CharField(max_length=1000, blank=True, null=True)
    filter_is_deleted = models.NullBooleanField(blank=True, null=True)

