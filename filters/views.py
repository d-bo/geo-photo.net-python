# -*- coding: utf-8 -*-

from filters.models import filters
from django.http import HttpResponse
import urllib2
import json
import time


def route(request, user_id):
    
    """ Debug route """
    
    return HttpResponse(user_id)


def create(request):
    
    """ Create filter """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            if request.POST.has_key('name') and request.POST.has_key('data'):
                name = request.POST['name']
                if len(name) > 0:
                    from front_end.views import load_session
                    _time = time.time()
                    load_session(request)
                    out = filters(filter_name = name, filter_user_id = request.user.id, filter_project_id = request.session['data']['working_project'], filter_create_date = _time, filter_data = request.POST['data'])
                    _out = out
                    out.save()
                    
                    return HttpResponse(json.dumps(get_filters(request)))


def update(request):
    
    """ Update filter """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            if request.POST.has_key('name') and request.POST.has_key('data'):
                name = request.POST['name']
                if len(name) > 0:
                    from front_end.views import load_session
                    _time = time.time()
                    load_session(request)
                    
                    out = filters.objects.filter(filter_name = urllib2.unquote(name), filter_user_id = request.user.id, filter_project_id = request.session['data']['working_project'])
                    
                    for z in out:
                        z.filter_data = request.POST['data']
                        z.save()
                    
                    return HttpResponse(json.dumps(get_filters(request)))


def switch(request):
    
    """ Switch filter """
    
    if request.user.is_authenticated():
        if request.method == 'GET':
            if request.GET.has_key('name'):
                name = request.GET['name']
                if len(name) > 0:
                    from front_end.views import load_session
                    _time = time.time()
                    load_session(request)
                    
                    out = filters.objects.filter(filter_name = urllib2.unquote(name), filter_user_id = request.user.id, filter_project_id = request.session['data']['working_project'])
                    
                    for z in out:
                        try:
                            data = json.loads(z.filter_data)
                            
                        except:
                            return HttpResponse('1')
                        else:
                            if data['enabled'] == True:
                                data['enabled'] = False
                            else:
                                data['enabled'] = True
                            
                            z.filter_data = json.dumps(data)
                            
                            z.save()
                    
                    return HttpResponse(json.dumps(get_filters(request)))


def delete(request):
    
    """ Delete filter """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            if request.POST.has_key('data'):
                try:
                    names = json.loads(request.POST['data'])
                except:
                    return HttpResponse(1)
                else:
                    from front_end.views import load_session
                    from datetime import datetime
                    time = datetime.now()
                    load_session(request)
                    
                    for x in names:
                        out = filters.objects.filter(filter_name = urllib2.unquote(x), filter_user_id = request.user.id, filter_project_id = request.session['data']['working_project'])
                        for z in out:
                            z.filter_is_deleted = True
                            z.save()
                    
                    return HttpResponse(json.dumps(get_filters(request)))


def load(request):
    
    """ Debug load filter """
    
    return HttpResponse('loaded')


def get_filters(request, shared=False, shared_user_id=False):
    
    """ Get filters """
    
    if request.user.is_authenticated():
        from front_end.views import load_session
        
        load_session(request)
        try:
            records = filters.objects.filter(filter_user_id = request.user.id, filter_project_id = request.session['data']['working_project'], filter_is_deleted=None)
        except:
            return {}
        else:
            out = {}
            
            for x in records:
                out[x.filter_name] = {}
                out[x.filter_name]['geodata'] = {}
                if x.filter_data:
                    try:
                        out[x.filter_name]['geodata'] = json.loads(x.filter_data)
                    except:
                        out[x.filter_name]['geodata'] = {}
                    else:
                        pass
            if out:
                return out
            else:
                return {}
        
