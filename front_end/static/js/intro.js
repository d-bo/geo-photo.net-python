
geo.gp = function(obj)
{
	var curleft = curtop = 0
	if (obj.offsetParent)
	{
		do {
			curleft += obj.offsetLeft
			curtop += obj.offsetTop
		} while (obj = obj.offsetParent)
		return [curleft, curtop]
	}
}

geo.cp = function(key)
{
	var b = document.getElementById('c_pic')
	var c = document.getElementById('c_user')
	var d = document.getElementById('container')
	
	var a = geo.gp(b)
	
	b.style.opacity = '0.2';
	
	if (document.getElementById('_span') == null)
	{
		var div = document.createElement('div')
		div.style.position = 'absolute'
		div.id = '_span'
		div.style.color = '#fff'
		div.style.fontSize = '30px'
		div.style.width = "128px"
		div.style.padding = '7px 12px'
		div.style.letterSpacing = '-2px'
		div.innerHTML = '<img src="images/76.gif" />'
		d.appendChild(div)
		var bc = (Math.ceil((b.offsetWidth) / 2) + a[0]) - (div.offsetWidth / 2)
		var bh = (Math.ceil(b.offsetHeight / 2) + a[1]) - (div.offsetHeight / 2)
		div.style.left  = bc + 'px'
		div.style.top   = bh + 'px'
		
	} else {
		
		d.removeChild(div);
		delete div;
		delete z;
	}
	c.innerHTML = this.pic_data[key]['name'];
	b.onload = function() {
		
		geo.geocode(key);
		b.style.opacity = '1';
		d.removeChild(div);
		delete div;
		delete z;
	};
	b.src = this.pic_data[key]['path'] + '&w=450&h=350&zc=1';
}

geo.geocode = function(key)
{
	var _lat, _lng, _gpslat, _gpslng, _href;
	document.getElementById('c_date').innerHTML = '<b>' + geo.pic_data[key].date + '</b>';
	href = 'http://geo-photo.net/id' + geo.pic_data[key].id;
	if (geo.pic_data[key].url.length > 1) href += '/' + geo.pic_data[key].url;
	document.getElementById('c_link').href = href;
	var geocoder = new google.maps.Geocoder();
	geocoder.geocode({'latLng': new google.maps.LatLng(parseFloat(geo.pic_data[key]['lat']), parseFloat(geo.pic_data[key]['lng']))}, function(results, status) {
		
		if (status == google.maps.GeocoderStatus.OK)
		{
			geo.pic_data[key].loc = results[0].formatted_address;
			
		} else {
			_lat    = parseFloat(geo.pic_data[key]['lat']);
			_lng    = parseFloat(geo.pic_data[key]['lng']);
			_gpslat = parseFloat(geo.pic_data[key]['gpslat']);
			_gpslng = parseFloat(geo.pic_data[key]['gpslng']);
			if (_lat != 0 && _lng != 0) geo.pic_data[key].loc = 'Lat: <b>' + _lat + '</b> Lng: <b>' + _lng + '</b>';
			else if (_gpslat != 0 && _gpslng != 0) geo.pic_data[key].loc = 'Lat: <b>' + _gpslat + '</b> Lng: <b>' + _gpslng + '</b>'; else geo.pic_data[key].loc = 'Location not detected';
		}
		document.getElementById('c_loc').innerHTML = geo.pic_data[key].loc
	})
}

geo.init = function()
{
	var img = new Image()
	img.src = 'images/76.gif'
	geo.geocode(0)
	document.getElementById('_box').style.opacity = '1'
}

var Sitis = {
	
	twitter_auth: function()
	{
		var twitter_popup = window.open("http://deploy.geo-photo.net/oauth/twitter","twit_window","width=700,height=630,menubar=1,resizable=0");
	},
	google_auth: function()
	{
		var google_popup = window.open("http://deploy.geo-photo.net/oauth/google","google_window","width=700,height=630,menubar=1,resizable=0");
	},
	facebook_auth: function()
	{
		var facebook_popup = window.open("http://deploy.geo-photo.net/oauth/facebook","facebook_window","width=700,height=630,menubar=1,resizable=0");
	},
	geophoto_register: function()
	{
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			method: 'GET',
			url: 'pages/sign.up.php',
			header: headers,
			success: function(resp)
			{
				document.getElementById('_box').innerHTML = resp.responseText
				window.rnd1 = Math.floor(Math.random()*11)
				window.rnd2 = Math.floor(Math.random()*12)
				document.getElementById('cptch').innerHTML = '<div style="width: 220px;text-align: left;color: #fff;padding-top: 10px">Check sum <b>'+window.rnd1+ '</b> + <b>'+window.rnd2+'</b> '+'</div><div><input type="text" class="inp" id="c_summ" value="" maxlength="3" autocomplete="off" style="width: 40px;margin-left: 10px"></div>'
			}
		})
	},
	load_login: function()
	{
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			method: 'GET',
			type: 'text',
			url: 'pages/sign.in.php',
			header: headers,
			success: function(resp)
			{
				document.getElementById('_box').innerHTML = resp.responseText
				document.getElementById('_box').style.opacity = '1'
			}
		})
	},
	check_sign_up: function()
	{
		err_arr = [
			'* Login must be 4 - 20 characters length',
			'* Invalid e-mail address',
			'* Password must be 4 - 20 characters length',
			'* Confirm password wrong',
			'* Wrong check sum'
		]
		
		err   = []
		var lgn   = document.getElementById('reg_lgn').value
		var email = document.getElementById('reg_email').value
		var pwd   = document.getElementById('reg_pwd').value
		var pwd_c = document.getElementById('reg_pwd_confirm').value
		var check = Sitis.validate_email(email)
		if (lgn.length < 4 || lgn.length > 20)
		{
			err.push(err_arr[0])
			document.getElementById('reg_lgn').style.background = 'brown'
		} else {document.getElementById('reg_lgn').style.background = '#282828';}
		
		if (!check) {
			err.push(err_arr[1]);
			document.getElementById('reg_email').style.background = 'brown';
		} else {document.getElementById('reg_email').style.background = '#282828';}
		if (pwd.length < 4 || pwd.length > 20) {
			err.push(err_arr[2]);
			document.getElementById('reg_pwd').style.background = 'brown';
		} else {document.getElementById('reg_pwd').style.background = '#282828';}
		if (pwd != pwd_c) {
			err.push(err_arr[3]);
			document.getElementById('reg_pwd_confirm').style.background = 'brown';
		} else {document.getElementById('reg_pwd_confirm').style.background = '#282828';}
		if ((window.rnd1+window.rnd2) != document.getElementById('c_summ').value) {
			err.push(err_arr[4]);
			document.getElementById('c_summ').style.background = 'brown';
		} else {document.getElementById('c_summ').style.background = '#282828';}
		if (err.length > 0) {
			//alert('error');
			alert('Please, fill required fields properly')
		}
		
		if (lgn.length > 0) _lgn = lgn; else _lgn = 0;
		if (pwd.length > 0) _pwd = hex_md5(pwd); else _pwd = 0;
		if (email.length > 0) _email = email; else _email = 0;
		if (pwd_c.length > 0) _pwd_c = hex_md5(pwd_c); else _pwd_c = 0;
		
		if (err.length < 1)
		{
			var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
			yajax.send({
				method: 'POST',
				type: 'text',
				url: '/account/register/',
				data: {"lgn": encodeURIComponent(_lgn), "email": encodeURIComponent(_email), "pwd": _pwd, "check": _pwd_c, "0": lgn.length, "1": email.length, "2": pwd.length, "3" : check.length},
				"header": headers,
				success: function(resp)
				{
					if (resp != 0)
					{
						var data = JSON.parse(resp)
						if ((data['double_login'] > 0 && data['double_email'] > 0) || (data['double_login'] > 0 && data['double_email'] < 1))
						{
							alert('Login is already in use. Please try another.')
						}
						if (typeof data['double_login'] == 'undefined' && data['double_email'] > 0)
						{
							alert('EMail is already in use. Please try another.')
						}
						if (data['double_login'] > 0 && data['double_email'] > 0)
						{
							alert('EMail or login is already in use. Please try another.')
						}
					}
					else
					{
						document.getElementById('reg_lgn').value = ''
						document.getElementById('reg_email').value = ''
						document.getElementById('reg_pwd').value = ''
						document.getElementById('reg_pwd_confirm').value = ''
						document.getElementById('c_summ').value = ''
						alert('Confirmation code was sent to your email.')
						//Sitis.load_login()
					}
				}
			})
		}
	},
	validate_email: function(email)
	{
		var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
		if (reg.test(email) == false) return false; else return true;
	}
};

/*
geo.keyEnter = function(k)
{
	switch(k)
	{
		case 1:
		geo.sign_in()
		break
		
		case 0:
		Sitis.check_sign_up()
		break
	}
}
*/

document.body.onkeydown = function(e)
{
	var keyCode = (window.Event) ? e.which : e.keyCode
	if (keyCode == 13)
	{
		if (document.getElementById('reg_pwd_confirm').length > 0) geo.keyEnter(0); else geo.keyEnter(1);
	}
}

window.rnd1 = Math.floor(Math.random()*11)
window.rnd2 = Math.floor(Math.random()*12)
document.getElementById('cptch').innerHTML = '<div style="text-align: left;color: #fff;padding-top: 10px">Check sum <b>'+window.rnd1+ '</b> + <b>'+window.rnd2+'</b> '+'<input type="text" class="inp" id="c_summ" value="" maxlength="3" autocomplete="off" style="width: 40px;margin-left: 10px"></div>'
