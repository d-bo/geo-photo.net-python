var canvas, context;

var tool
var tool_default = 'rect'

function init ()
{
	canvas = document.getElementById('canvas-frame')
	if (!canvas)
	{
	  alert('Error: I cannot find the canvas element!')
	  return;
	}

    if (!canvas.getContext) {
      alert('Error: no canvas.getContext!')
      return;
    }

    context = canvas.getContext('2d');
    if (!context) {
      alert('Error: failed to getContext!')
      return;
    }
	
	tool = new tools[tool_default]()
    canvas.addEventListener('mousedown', ev_canvas, false)
    canvas.addEventListener('mousemove', ev_canvas, false)
    canvas.addEventListener('mouseup',   ev_canvas, false)
}

function ev_canvas (ev)
{
	if (ev.layerX || ev.layerX == 0)
	{
		ev._x = ev.layerX
		ev._y = ev.layerY
	} else if (ev.offsetX || ev.offsetX == 0) {
		ev._x = ev.offsetX
		ev._y = ev.offsetY
	}

	var func = tool[ev.type]
	if (func)
	{
	  func(ev)
	}
}

  function ev_tool_change (ev) {
    if (tools[this.value]) {
      tool = new tools[this.value]();
    }
  }

  var tools = {};

  tools.rect = function ()
	{
    var tool = this;
    this.started = false;
	
    this.mousedown = function (ev) {
      tool.started = true;
      tool.x0 = ev._x;
      tool.y0 = ev._y;
    };
	
    this.mousemove = function (ev) {
	if (!tool.started)
	{
		return;
	}
	
      var x = Math.min(ev._x,  tool.x0),
          y = Math.min(ev._y,  tool.y0),
          w = Math.abs(ev._x - tool.x0),
          h = Math.abs(ev._y - tool.y0);
	
	context.clearRect(0, 0, canvas.width, canvas.height)
	
	if (!w || !h)
	{
		return
	}
	context.strokeStyle="#FF0000";
	context.lineWidth = 5;
	context.fillStyle = "rgba(255, 255, 255, 0.5)"
	context.strokeRect(x, y, w, h);
	context.fillRect(x, y, w, h);
     
	window.x  = tool.x0
	window.y  = tool.y0
	window.x1 = ev._x
	window.y1 = ev._y
	
	};

    this.mouseup = function (ev) {
      if (tool.started) {
        tool.mousemove(ev);
        tool.started = false;
      }
    };
  };

