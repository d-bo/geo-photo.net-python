
window.maps = {}

var geo = {
	
	opts: {
		'csrf_cookie': 'csrftoken'
	},
	spin: {
		lines: 13, // The number of lines to draw
		length: 15, // The length of each line
		width: 7, // The line thickness
		radius: 19, // The radius of the inner circle
		corners: 1, // Corner roundness (0..1)
		rotate: 0, // The rotation offset
		color: '#fff', // #rgb or #rrggbb
		speed: 1, // Rounds per second
		trail: 60, // Afterglow percentage
		shadow: true, // Whether to render a shadow
		hwaccel: false, // Whether to use hardware acceleration
		className: 'spinner', // The CSS class to assign to the spinner
		zIndex: 2e9, // The z-index (defaults to 2000000000)
		top: 'auto', // Top position relative to parent in px
		left: 'auto' // Left position relative to parent in px
	}
}

geo.id = function(a)
{
	if (a) return document.getElementById(a); else return false;
}

geo.helpers = {
	
	bindEvent: function(elem ,evt, callback)
	{
		if (elem.addEventListener)
		{
			elem.addEventListener(evt, callback, false)
		}
		else if	(elem.attachEvent)
		{
			elem.attachEvent('on' + evt, function() {
				callback.call(event.srcElement,event)
			})
		}
	},
	readCookie: function(name)
	{
		var nameEQ = name + "="
		var ca = document.cookie.split(';')
		for (var i = 0; i < ca.length; i++)
		{
			var c = ca[i]
			while (c.charAt(0) == ' ') c = c.substring(1, c.length)
			if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length)
		}
		return null
	},
	setCookie: function(name, value, props)
	{
		props = props || {}
		var exp = props.expires
		if (typeof exp == "number" && exp)
		{
			var d = new Date()
			d.setTime(d.getTime() + exp * 1000)
			exp = props.expires = d
		}
		if (exp && exp.toUTCString) { props.expires = exp.toUTCString() }
		
		value = encodeURIComponent(value)
		var updatedCookie = name + "=" + value
		for (var propName in props)
		{
			updatedCookie += "; " + propName
			var propValue = props[propName]
			if (propValue !== true) { updatedCookie += "=" + propValue }
		}
		document.cookie = updatedCookie
	},
	obj_size: function(obj)
	{
		var count = 0;
		for(var prop in obj)
		{
			count++;
		}
		return count;
	},
	timestamp: function()
	{
		return new Date().getTime()
	}
}

function appendExt(type, src, callback)
{
	var head = document.getElementsByTagName('head')
	if ( head.length == 0 )
	{
		return;
	}
	
	var z;
	if ( document.createElementNS && head[0].tagName == 'head' ) {
		z = document.createElementNS( 'http://www.w3.org/1999/xhtml',
			type == 'css' ? 'link' : 'script' );
	}
	else {
		z = document.createElement( type == 'css' ? 'link' : 'script' );
	}

	//z.id = id;
	z.type = type == 'css' ? 'text/css' : 'text/javascript';
	if ( type == 'css' ) {
		z.rel = 'stylesheet';
		z.href = baseUrl + src;
	}
	else if ( type == 'script' ) {
		z.src = src;
	}
	head[0].appendChild( z );

	if ( callback ) {
		z.onload = callback;
		function wait()
		{
			callback(bc)
		}
		var bc = setInterval(wait, 100)
		
		var callback_count = 0;
		z.onreadystatechange = function () { if ( z.readyState == 'loaded' || z.readyState == 'complete' )
		{
			function wait()
			{
				callback(bc)
			}
			var bc = setInterval(wait, 100)
		} };
	}
}

/**
 * Kinda JSONP script loading + script execution checking
 * (don't work well when the loaded script try to load other scripts
 *  by using document.write, when the page is already loaded)
 */

geo.load_script = function(url, callback)
{
	appendExt( 'script', url, callback )
}

geo._load_script = function(url, callback)
{
	var head, script, exec
	
	head = document.getElementsByTagName('head')[0]
	script = document.createElement('script')
	exec = callback
	script.onload = function()
	{
		function wait()
		{
			callback(bc)
		}
		var bc = setInterval(wait, 100)
	}
	script.type= 'text/javascript'
	script.src= url
	head.appendChild(script)
}

/**
 * Compute markers arc
 */

geo.calc_additional_params = function(data)
{
	var lat, lng, start, rotation, _roll, moffset, mnumarcs, p_t, spherical, angle_src
	
	moffset  = geo.get('marker-step-size')
	mnumarcs = geo.get('marker-grid-steps')
	
	if (!moffset)
	{
		moffset = 20
		geo.set('marker-step-size', moffset)
	}
	
	if (!mnumarcs)
	{
		mnumarcs = 5
		geo.set('marker-grid-steps', mnumarcs)
	}
	
	if (data)
	{
		// from google maps geometry plugin
		spherical = google.maps.geometry.spherical
		
		var z = 0
		for (var x in data)
		{
			data[x]['filename'] = x
			if (data[x]['gps_lat'] != null || data[x]['gps_lat'] != '0') lat = data[x]['gps_lat']
			if (data[x]['user_lat'] != null || data[x]['user_lat'] != '0') lat = data[x]['user_lat']
			if (data[x]['lat'] != null || data[x]['lat'] != '0') lat = data[x]['lat']
			
			if (data[x]['gps_lng'] != null || data[x]['gps_lng'] != '0') lng = data[x]['gps_lng']
			if (data[x]['user_lng'] != null || data[x]['user_lng'] != '0') lng = data[x]['user_lng']
			if (data[x]['lat'] != null || data[x]['lng'] != '0') lng = data[x]['lng']
			
			if ((lat != 0 && lat != null) && (lng != 0 && lng != null))
			{
				p_t = 90 - Math.atan(0.5) * 180 / Math.PI
				start = new google.maps.LatLng(lat, lng)
				rotation = 0
				
				data[x]['lat'] = lat
				data[x]['lng'] = lng
				
				if (Math.abs(data[x]['pitch']) <= p_t)
				{
					_roll = Math.round(data[x]['roll'])
					if (_roll < 0) _roll += 360
					_roll = Math.floor((_roll + 45) / 90) * 90
					rotation = _roll % 360
				}
				
				if (rotation == 90 || rotation == 270) angle_src = data[x]['v_angle']; else angle_src = data[x]['h_angle'];
				start = new google.maps.LatLng(lat, lng)
				
				var line_length = (moffset * mnumarcs) + 10
				var end_angle   = parseFloat(data[x]['az']) + (angle_src / 2)
				var begin_angle = parseFloat(data[x]['az']) - (angle_src / 2)
				
				data[x]['start']  = start
				data[x]['end_l']  = spherical.computeOffset(start, line_length, end_angle)
				data[x]['end_l1'] = spherical.computeOffset(start, line_length, begin_angle)
				data[x]['end_l2'] = spherical.computeOffset(start, line_length, data[x]['az'])
				
				data[x]['coord'] = [data[x]['start'], data[x]['end_l']]
				data[x]['coord1'] = [data[x]['start'], data[x]['end_l1']]
				data[x]['coord2'] = [data[x]['start'], data[x]['end_l2']]
				
				if (mnumarcs != '0')
				{
					data[x]['arc'] = []
					for (var i = 0; i <= mnumarcs; i++)
					{
						var k = 0
						data[x]['arc'][i] = []
						var offset = i * moffset
						for (var zz = begin_angle; zz <= end_angle; zz++)
						{
							data[x]['arc'][i][k] = new google.maps.geometry.spherical.computeOffset(start, offset, zz)
							k++
						}
					}
				}
			}
			z++
		}
		data['total'] = z
		//console.debug(data)
		return data
	}
	else
	{
		alert('calc_additional_params: no data received')
	}
}

/**
 * Render google map
 */

geo.map_render_google = function(div)
{
	var _div = div
	
	var type  = geo.get(div + '-google-type')
	if (type == 0 || type == null)
	{
		type = google.maps.MapTypeId.ROADMAP
		geo.set(div + '-google-type', '0')
	}
	
	var working_image = escape(geo.get('working_image'))
	
	var el = geo.id(div)
	el.style.padding='3px'
	el.style.background = 'url(/static/img/ste.png)'
	el.innerHTML = ''
	
	if (!window.markers_stack[working_image]['start'])
	{
		geo.id(div).innerHTML = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px;font-size: 16px;color: #fff">No geo data</div>'
		window.maps[div] = ''
		return
	}
	
	var latlng = new google.maps.LatLng(parseFloat(window.markers_stack[working_image]['lat']), parseFloat(window.markers_stack[working_image]['lng']))
	
	var gzoom = geo.get(div + '-google-zoom')
	if (gzoom == 0 || gzoom == null || gzoom == 'undefined')
	{
		var gzoom = 10
		geo.set(div + '-google-zoom', gzoom)
	}
	
	var mapOptions = {
		zoom: parseInt(gzoom),
		center: latlng,
		scaleControl: true,
		disableDefaultUI: false,
		mapTypeId: type
	}
	
	if (window.maps[div]) window.maps[div].length = 0; else window.maps[div] = {};
	window.maps[div]  = new google.maps.Map(el, mapOptions)
	
	google.maps.event.addListener(window.maps[div], 'maptypeid_changed', function(){
		var g_type = window.maps[div].getMapTypeId()
		if (g_type == 'roadmap') geo.set(div + '-google-type', '0'); else geo.set(div + '-google-type', '1');
		//gCookie = Cookie.write('_'+divBlock, g_type, {duration: 1, path: "/", domain: '.geo-photo.net'});
	})
	
	google.maps.event.addListener(window.maps[div], 'zoom_changed', function(){
		var gzoom = window.maps[div].getZoom()
		geo.set(div + '-google-zoom', gzoom)
	})
	
	// mode button need to be rendered before marker set
	if (!geo.id('canvas-azimuth')) geo.widget_edit_render()
	
	var z = 0
	
	// single or group mode
	var mode = geo.get(div + '-mode')
	if (mode == 'single')
	{
		geo.map_set_marker('google', window.maps[div], window.markers_stack[working_image], false, _div)
	}
	else
	{
		for (var i in window.markers_stack)
		{
			if (window.markers_stack[i]['start'])
			{
				geo.map_set_marker('google', window.maps[div], window.markers_stack[i], false, _div)
				geo.id('b1-count').innerHTML = z
				z++
			}
		}
	}
	window.maps[div].setCenter(latlng)
	if (div == 'b1') geo.render_filters()
	return [parseFloat(window.markers_stack[working_image]['lat']), parseFloat(window.markers_stack[working_image]['lng'])]
}

/**
 * Switch map display mode for current div
 */

geo.switch_mode = function(div)
{
	var mode = geo.get(div + '-mode')
	if (mode == 'single') geo.set(div + '-mode', 'multi'); else geo.set(div + '-mode', 'single');
	var map = geo.get(div)
	switch(map)
	{
		case('yandex'):
			geo.loading_progress(div)
			geo.load_yandex(div)
			break
		case('bing'):
			geo.loading_progress(div)
			geo.load_bing(div)
			break
		case('google'):
			geo.loading_progress(div)
			geo.load_google(div)
			break
		case('osm'):
			geo.loading_progress(div)
			geo.load_osm(div)
			break
	}
}

/**
 * Set marker(s) on the maps
 */

geo.map_set_marker = function(type, map_instance, obj, additional, div)
{
	var moffset, mnumarcs, latlng, adv_mode, coords, mode, img, colors
	
	latlng = [obj['lat'], obj['lng']]
	adv_mode = obj['advance-mode']
	
	coords = new google.maps.LatLng(latlng[0], latlng[1])
	
	moffset = geo.get('marker-step-size')
	mnumarcs = geo.get('marker-grid-steps')
	
	img = escape(geo.get('working_image'))
	
	if (geo.id(div + '-marker-mode-button'))
	{
		mode = geo.id(div + '-marker-mode-button').getAttribute("switched")
	}
	else
	{
		mode = geo.get(div + '-marker-mode-advanced')
	}
	
	// filters
	if (geo.helpers.obj_size(window.filters_stack) > 0)
	{
		if (geo.get('filters') == 'on')
		{
			if (!geo.filters_grant_view(obj)) return
		}
	}
	
	switch(type)
	{
		
		/**
		 * YANDEX
		 */
		
		case('yandex'):
			
			var img_icon = '/static/img/mark.png'
			var center = new YMaps.GeoPoint(obj.coord[0].lng(), obj.coord[0].lat())
			
			if (mode == 'on')
			{
				if (img == obj['filename'])
				{
					colors = ['#0191C8', '#ff0000']
					var zindex = 2000
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				else
				{
					colors = ['#9B9CA3', '#9B9CA3']
					var zindex = 1000
				}
				
				var line1 = new ymaps.GeoObject({
						geometry: {
							type: "LineString",
							coordinates: [
								[obj.coord[0].lat(), obj.coord[0].lng()], [obj.coord[1].lat(), obj.coord[1].lng()]
							]
						},
						properties: {
							hintContent: "line"
						}
					}, {
						geodesic: true,
						strokeWidth: 2,
						strokeColor: colors[0]
					});
				map_instance.geoObjects.add(line1)
				
				var line2 = new ymaps.GeoObject({
						geometry: {
							type: "LineString",
							coordinates: [
								[obj.coord1[0].lat(), obj.coord1[0].lng()], [obj.coord1[1].lat(), obj.coord1[1].lng()]
							]
						},
						properties: {
							hintContent: "line"
						}
					}, {
						geodesic: true,
						strokeWidth: 2,
						strokeColor: colors[0]
					});
				map_instance.geoObjects.add(line2)
				
				var line3 = new ymaps.GeoObject({
						geometry: {
							type: "LineString",
							coordinates: [
								[obj.coord2[0].lat(), obj.coord2[0].lng()], [obj.coord2[1].lat(), obj.coord2[1].lng()]
							]
						},
						properties: {
							hintContent: "line"
						}
					}, {
						geodesic: true,
						strokeWidth: 2,
						strokeColor: colors[1]
					});
				map_instance.geoObjects.add(line3)
				
				var mark = new ymaps.GeoObject({
						geometry: {
							type: "Point",
							coordinates: 
								[obj.coord[0].lat(), obj.coord[0].lng()]
							
						}
					}, {
						iconImageHref: img_icon,
						iconImageSize: [17,17],
						iconImageOffset: [-9,-9]
					});
				map_instance.geoObjects.add(mark)
				
				var loc  = []
				var line = []
				var soz  = []
				
				for (var x = 1;x <= mnumarcs; x++)
				{
					for (var z = 0;z < obj.arc[x].length; z++)
					{
						var _lat   = obj.arc[x][z]
						var _lng   = obj.arc[x][z]
						var b_lat  = _lat.lat()
						var b_lng  = _lng.lng()
						loc[z] = [b_lat, b_lng]
					}
					
					soz[x]      = loc
					loc         = []
					line[x+'y'] = new ymaps.GeoObject({
						geometry: {
							type: "LineString",
							coordinates: soz[x]
						},
						properties: {
							hintContent: "line"
						}
					}, {
						geodesic: true,
						strokeWidth: 1,
						strokeColor: colors[0]
					})
					//line[x+'y'].setStyle("yamap#CustomLineU")
					map_instance.geoObjects.add(line[x+'y'])
				}
			}
			else
			{
				if (img == obj['filename'])
				{
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				var mrk = new ymaps.Placemark([latlng[0], latlng[1]])
				map_instance.geoObjects.add(mrk)
			}
			
			break
			
		/**
		 * GOOGLE
		 */
		
		case('google'):
			
			// "advanced mode"
			// colors [ff0000, 0191C8]
			
			var img_icon = '/static/img/mark.png'
			
			var image = new google.maps.MarkerImage(
				img_icon,
				new google.maps.Size(17, 17),
				new google.maps.Point(0, 0),
				new google.maps.Point(8, 8)
			)
			
			if (mode == 'on')
			{
				if (img == obj['filename'])
				{
					colors = ['#0191C8', '#ff0000']
					var zindex = 2000
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				else
				{
					colors = ['#9B9CA3', '#9B9CA3']
					var zindex = 1000
				}
				
				var image = new google.maps.MarkerImage(
					img_icon,
					new google.maps.Size(17, 17),
					new google.maps.Point(0, 0),
					new google.maps.Point(8, 8)
				)
				
				new google.maps.Polyline({
					path: obj['coord'],
					map: map_instance,
					strokeColor: colors[0],
					strokeOpacity: 1.0,
					strokeWeight: 2,
					zIndex: zindex
				})
				
				new google.maps.Polyline({
					path: obj['coord1'],
					map: map_instance,
					strokeColor: colors[0],
					strokeOpacity: 1.0,
					strokeWeight: 2,
					zIndex: zindex
				})
				
				new google.maps.Polyline({
					path: obj['coord2'],
					map: map_instance,
					strokeColor: colors[1],
					strokeOpacity: 1.0,
					strokeWeight: 2,
					zIndex: zindex
				})
				
				if (typeof(obj.arc[0]) == 'object')
				{
					for (var xo = 0; xo <= mnumarcs; xo++)
					{
						new google.maps.Polyline({
							path: obj.arc[xo],
							map: map_instance,
							strokeColor: colors[0],
							strokeOpacity: 1.0,
							strokeWeight: 1,
							zIndex: zindex
						})
					}
				}
				
				var marker = new google.maps.Marker({
					position: coords,
					icon: image,
					zIndex: zindex
				})
				marker.setMap(map_instance)
			}
			else
			{
				if (img == obj['filename'])
				{
					var marker = new google.maps.Marker({
						position: coords,
						zIndex: zindex
					})
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				else
				{
					var marker = new google.maps.Marker({
						position: coords,
						zIndex: zindex,
						icon: image
					})
				}
				marker.setMap(map_instance)
			}
			break
			
		/**
		 * BING
		 */
			
		case('bing'):
			
			var img_icon = '/static/img/mark.png'
			var center = new Microsoft.Maps.Location(latlng[0], latlng[1])
			
			if (mode == 'on')
			{
				if (img == obj['filename'])
				{
					colors = ['#0191C8', '#ff0000']
					var zindex = 2000
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				else
				{
					colors = ['#9B9CA3', '#9B9CA3']
					var zindex = 1000
				}
				
				var location1 = new Microsoft.Maps.Location(obj.coord[0].lat(), obj.coord[0].lng())
				var location1e = new Microsoft.Maps.Location(obj.coord[1].lat(), obj.coord[1].lng())
				var location2 = new Microsoft.Maps.Location(obj.coord1[0].lat(), obj.coord1[0].lng())
				var location2e = new Microsoft.Maps.Location(obj.coord1[1].lat(), obj.coord1[1].lng())
				var location3 = new Microsoft.Maps.Location(obj.coord2[0].lat(), obj.coord2[0].lng())
				var location3e = new Microsoft.Maps.Location(obj.coord2[1].lat(), obj.coord2[1].lng())
				
				var lineVertices  = new Array(location1, location1e)
				var lineVertices1 = new Array(location2, location2e)
				var lineVertices2 = new Array(location3, location3e)
				var line = new Microsoft.Maps.Polyline(lineVertices, {strokeColor: new Microsoft.Maps.Color.fromHex(colors[0])})
				var line1 = new Microsoft.Maps.Polyline(lineVertices1, {strokeColor: new Microsoft.Maps.Color.fromHex(colors[0])})
				var line2 = new Microsoft.Maps.Polyline(lineVertices2, {strokeColor: new Microsoft.Maps.Color.fromHex(colors[1])})
				
				map_instance.entities.push(line)
				map_instance.entities.push(line1)
				map_instance.entities.push(line2)
				
				var loc  = []
				var line = []
				var soz  = []
				
				var layer = new Microsoft.Maps.EntityCollection()
				layer.setOptions({visible: true, zIndex: 1001})
				map_instance.entities.push(layer)
				layer.setOptions({visible: true})
				
				for (var x = 1;x <= mnumarcs; x++)
				{
					for (var z = 0;z < obj.arc[x].length; z++)
					{
						var _lat   = obj.arc[x][z]
						var _lng   = obj.arc[x][z]
						var b_lat  = _lat.lat()
						var b_lng  = _lng.lng()
						loc[z] = new Microsoft.Maps.Location(b_lat, b_lng)
					}
					
					soz[x] = loc
					loc    = []
					line[x+'bing'] = new Microsoft.Maps.Polyline(soz[x], {strokeColor:new Microsoft.Maps.Color.fromHex(colors[0]), strokeThickness: 1, strokeDashArray:"8 4"})
					map_instance.entities.push(line[x+'bing'])
				}
				
				var marker = new Microsoft.Maps.Pushpin(center, {icon: img_icon, anchor: new Microsoft.Maps.Point(8,8), draggable: false})
				map_instance.entities.push(marker)
			}
			else
			{
				if (img == obj['filename'])
				{
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				var marker = new Microsoft.Maps.Pushpin(center, {anchor: new Microsoft.Maps.Point(8,8), draggable: false})
				map_instance.entities.push(marker)
			}
			
			break
		
		/**
		 * OSM
		 */
		
		case('osm'):
			
			map_instance.addLayer(additional)
			var center = new OpenLayers.LonLat(latlng[1], latlng[0]).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject())
			var img_icon = '/static/img/mark.png'
			if (mode == 'on')
			{
				//alert(img + ' / ' + obj['filename'])
				if (img == obj['filename'])
				{
					colors = ['#0191C8', '#ff0000']
					var zindex = 2000
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				else
				{
					colors = ['#9B9CA3', '#9B9CA3']
					var zindex = 1000
				}
				
				var lineLayer  = new OpenLayers.Layer.Vector("Line Layer")
				var lineLayer1 = new OpenLayers.Layer.Vector("Line Layer1")
				var lineLayer2 = new OpenLayers.Layer.Vector("Line Layer2")
				
				var style_red = {
					strokeColor: colors[1],
					strokeOpacity: 1,
					strokeWidth: 2
				}
				
				var style_blue  = {strokeColor: colors[0], strokeOpacity: 1, strokeWidth: 3}
				var style_blue1 = {strokeColor: colors[0], strokeOpacity: 1, strokeWidth: 1}
				
				var icon = new OpenLayers.Icon(img_icon, new OpenLayers.Size(17,17))
				var mark = new OpenLayers.Marker(center, icon)
				additional.addMarker(mark)
				
				var points  = []
				var points1 = []
				var points2 = []
				//console.debug(obj)
				points[0] = new OpenLayers.LonLat(obj.coord[0].lng(), obj.coord[0].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject());
				points[0] = new OpenLayers.Geometry.Point(points[0].lon,points[0].lat);
				
				points[1] = new OpenLayers.LonLat(obj.coord[1].lng(), obj.coord[1].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject());
				points[1] = new OpenLayers.Geometry.Point(points[1].lon, points[1].lat);
				
				points1[0] = new OpenLayers.LonLat(obj.coord1[0].lng(), obj.coord1[0].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject());
				points1[0] = new OpenLayers.Geometry.Point(points1[0].lon, points1[0].lat);
				
				points1[1] = new OpenLayers.LonLat(obj.coord1[1].lng(), obj.coord1[1].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject());
				points1[1] = new OpenLayers.Geometry.Point(points1[1].lon, points1[1].lat);
				
				points2[0] = new OpenLayers.LonLat(obj.coord2[0].lng(), obj.coord2[0].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject());
				points2[0] = new OpenLayers.Geometry.Point(points2[0].lon, points2[0].lat);
				
				points2[1] = new OpenLayers.LonLat(obj.coord2[1].lng(), obj.coord2[1].lat()).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject());
				points2[1] = new OpenLayers.Geometry.Point(points2[1].lon, points2[1].lat);
				
				var linear_ring = new OpenLayers.Geometry.LinearRing(points);
				polygonFeature = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring]), null, style_blue)
				lineLayer.addFeatures([polygonFeature])
				
				var linear_ring1 = new OpenLayers.Geometry.LinearRing(points1)
				var polygonFeature1 = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring1]), null, style_blue)
				lineLayer1.addFeatures([polygonFeature1])
				
				var linear_ring2 = new OpenLayers.Geometry.LinearRing(points2)
				var polygonFeature2 = new OpenLayers.Feature.Vector(new OpenLayers.Geometry.Polygon([linear_ring2]), null, style_red)
				lineLayer1.addFeatures([polygonFeature2])
				
				map_instance.addLayer(lineLayer)
				map_instance.addLayer(lineLayer1)
				map_instance.addLayer(lineLayer2)
				
				var loc  = []
				var line = []
				var soz  = []
				var lay  = []
				
				if (typeof(obj.arc[0]) != 'undefined')
				{
					for (var xu = 1;xu <= mnumarcs; xu++) {
						
						lay[xu] = new OpenLayers.Layer.Vector('vc'+xu);
						
						for (var zu = 0;zu < obj.arc[xu].length; zu++)
						{
							var _lat    = obj.arc[xu][zu]
							var _lng    = obj.arc[xu][zu]
							var b_lat   = _lat.lat()
							var b_lng   = _lng.lng()
							loc[zu] = new OpenLayers.LonLat(b_lng, b_lat).transform(new OpenLayers.Projection("EPSG:4326"), map_instance.getProjectionObject())
							loc[zu] = new OpenLayers.Geometry.Point(loc[zu].lon, loc[zu].lat)
						}
						
						soz[xu]   = loc
						loc       = []
						line[xu]  = new OpenLayers.Geometry.LineString(soz[xu])
						var lineFeature = new OpenLayers.Feature.Vector(line[xu], null, style_blue1)
						lay[xu].addFeatures([lineFeature])
						map_instance.addLayer(lay[xu])
					}
				}
			}
			else
			{
				if (img == obj['filename'])
				{
					map_instance['current_marker'] = [obj.lat, obj.lng]
				}
				var mark = new OpenLayers.Marker(center)
				additional.addMarker(mark)
			}
			
			break
	}
	
	return
}

/**
 * Render openstreet map
 */

geo.map_render_osm = function(div)
{
	var working_image = escape(geo.get('working_image'))
	
	var el = geo.id(div)
	//el.style.length = 0
	el.style.padding='3px'
	el.style.background = 'url(/static/img/ste.png)'
	el.innerHTML = ''
	
	if (!window.markers_stack[working_image]['start'])
	{
		geo.id(div).innerHTML = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px;font-size: 16px;color: #fff">No geo data</div>'
		window.maps[div] = ''
		return
	}
	
	var zoom = parseInt(geo.get(div + '-osm-zoom'))
	if (zoom == 0 || zoom == null || zoom == 'undefined')
	{
		zoom = 10
		geo.set(div + '-osm-zoom', zoom)
	}
	
	var _div = div
	
	OpenLayers.ImgPath = "/static/img/openlayers/"
	
	window.maps[div] = {}
	
	window.maps[div] = new OpenLayers.Map(div, {controls: [], eventListeners: {"zoomend": function(){
		var osmzoom = window.maps[_div].getZoom()
		geo.set(_div + '-osm-zoom', osmzoom)
	}}});
	
	window.maps[div].addControl(new OpenLayers.Control.Navigation())
	window.maps[div].addControl(new OpenLayers.Control.Attribution())
	window.maps[div].addControl(new OpenLayers.Control.ScaleLine())
	window.maps[div].addControl(new OpenLayers.Control.Navigation({"zoomWheelEnabled": true}))
	//map.addControl(new OpenLayers.Control.PanZoomBar());
	var maposm = new OpenLayers.Layer.OSM()
	window.maps[div].addLayer(maposm)
	
	var marker_layer = new OpenLayers.Layer.Markers("Markers")
	
	var z = 0
	if (!geo.id('canvas-azimuth')) geo.widget_edit_render()
	
	// single or group mode
	var mode = geo.get(div + '-mode')
	
	if (mode == 'single')
	{
		geo.map_set_marker('osm', window.maps[div], window.markers_stack[working_image], marker_layer, _div)
	}
	else
	{
		for (var i in window.markers_stack)
		{
			if (window.markers_stack[i]['start'])
			{
				geo.map_set_marker('osm', window.maps[div], window.markers_stack[i], marker_layer, _div)
				geo.id('b1-count').innerHTML = z
				z++
			}
		}
	}
	
	window.maps[div].setCenter(new OpenLayers.LonLat(parseFloat(window.markers_stack[working_image]['lng']), parseFloat(window.markers_stack[working_image]['lat']))
	.transform(
		new OpenLayers.Projection("EPSG:4326"),
		new OpenLayers.Projection("EPSG:900913")
	), zoom
	)
	geo.render_filters()
	return [parseFloat(window.markers_stack[working_image]['lat']), parseFloat(window.markers_stack[working_image]['lng'])]
}

/**
 * Render yandex map
 */

geo.map_render_yandex = function(div)
{
	//function render_yandex(_div)
	//{
	var _div = div
	
	var working_image = escape(geo.get('working_image'))
	
	var el = geo.id(div)
	//el.style.length = 0
	el.style.padding='3px'
	el.style.background = 'url(/static/img/ste.png)'
	el.innerHTML = ''
	if (!window.markers_stack[working_image]['start'])
	{
		geo.id(_div).innerHTML = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px;font-size: 16px;color: #fff">No geo data</div>'
		window.maps[div] = ''
		return
	}
	
	var yzoom = geo.get(div + '-yandex-zoom')
	if (yzoom == 0 || yzoom == null || yzoom == 'undefined')
	{
		yzoom = 10
		geo.set(div + '-yandex-zoom', yzoom)
	}
	
	//console.debug(window.markers_stack[working_image])
	window.maps[div]  = new ymaps.Map(div, {
		center: [window.markers_stack[working_image]['lat'], window.markers_stack[working_image]['lng']],
		zoom: parseInt(yzoom),
		behaviors: ["default", "scrollZoom"]
	});
	
	/*
	YMaps.Events.observe(window.maps[div], window.maps[div].Events.Update, function() {
		var yzoom = window.maps[div].getZoom()
		geo.set('-yandex-zoom', yzoom)
	});
	*/
	
	//console.debug(window.maps[div].events)
	
	window.maps[div].events.add("wheel", function() {
		var yzoom = window.maps[div].getZoom()
		geo.set(div + '-yandex-zoom', yzoom)
	})
	
	window.maps[div].controls
				.add('zoomControl')
                .add('typeSelector')
                .add('smallZoomControl', { right: 5, top: 75 })
                .add('mapTools')
	
	//alert(typeof YMaps.MapType)
	var type  = geo.get(div + '-yandex-type')
	if (type == 0 || type == null || type == 'undefined')
	{
		//window.maps[div].setType(YMaps.MapType.MAP)
		geo.set(div + '-yandex-type', '0')
	}
	else
	{
		//if (type == 0 || type == null) 
		if (type == 1)
		{
			window.maps[div].setType(YMaps.MapType.SATELLITE)
			geo.set(div + '-yandex-type', '1')
		}
		if (type == 2)
		{
			window.maps[div].setType(YMaps.MapType.HYBRID)
			geo.set(div + '-yandex-type', '2')
		}
	}
	
	//window.maps[div].setCenter(new YMaps.GeoPoint(window.markers_stack[working_image]['lng'], window.markers_stack[working_image]['lat']), yzoom)
	
	var z = 0
	
	if (!geo.id('canvas-azimuth')) geo.widget_edit_render()
	
	// single or group mode
	var mode = geo.get(div + '-mode')
	if (mode == 'single')
	{
		geo.map_set_marker('yandex', window.maps[div], window.markers_stack[working_image], false, _div)
	}
	else
	{
		for (var i in window.markers_stack)
		{
			if (window.markers_stack[i]['start'])
			{
				geo.map_set_marker('yandex', window.maps[div], window.markers_stack[i], false, _div)
				geo.id('b1-count').innerHTML = z
				z++
			}
		}
	}
	
	geo.render_filters()
	return [parseFloat(window.markers_stack[working_image]['lat']), parseFloat(window.markers_stack[working_image]['lng'])]
	//}
}

/**
 * Render bing map
 */

// key = "AstjYSutmzkzHb7M9deDBToueqFQ0Niz8G8eCHPb3Iwnpll2Gq0URgK-P4OI_kGQ"
// from account dmitry.vasechkin@gmail.com

geo.map_render_bing = function(div)
{
	var working_image = escape(geo.get('working_image'))
	
	var el = geo.id(div)
	//el.style.length = 0
	el.style.padding='3px'
	el.style.background = 'url(/static/img/ste.png)'
	el.innerHTML = ''
	
	if (!window.markers_stack[working_image]['start'])
	{
		geo.id(div).innerHTML = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px;font-size: 16px;color: #fff">No geo data</div>'
		window.maps[div] = ''
		return
	}
	
	var type  = geo.get(div + '-bing-type')
	
	if (type == 0 || type == null) type = Microsoft.Maps.MapTypeId.road; else type = Microsoft.Maps.MapTypeId.birdseye;
	
	var zoom = geo.get(div + '-bing-zoom')
	if (zoom == 0 || zoom == null || zoom == 'undefined')
	{
		zoom = 10
		geo.set(div + '-bing-zoom', zoom)
	}
	
	bingOptions = {
		credentials: "AstjYSutmzkzHb7M9deDBToueqFQ0Niz8G8eCHPb3Iwnpll2Gq0URgK-P4OI_kGQ",
		center: new Microsoft.Maps.Location(parseFloat(window.markers_stack[working_image]['lat']), parseFloat(window.markers_stack[working_image]['lng'])),
		mapTypeId: type,
		enableClickableLogo: false,
		enableSearchLogo: false,
		//showDashboard: false,
		showCopyright: false,
		//showScalebar: false,
		zoom: parseInt(zoom)
	}
	
	window.maps[div] = new Microsoft.Maps.Map(geo.id(div), bingOptions)
	
	var _div = div
	Microsoft.Maps.Events.addHandler(window.maps[div], 'viewchangeend', function() {
		var zoom = window.maps[div].getZoom()
		geo.set(div + '-bing-zoom', zoom)
	})
	
	Microsoft.Maps.Events.addHandler(window.maps[div], 'maptypechanged', function(e)
	{
		var g_type = window.maps[_div].getMapTypeId()
		if (g_type == 'r') geo.set(_div + '-bing-type', 0); else geo.set(_div + '-bing-type', 1)
	})
	
	if (!geo.id('canvas-azimuth')) geo.widget_edit_render()
	
	// single or group mode
	var mode = geo.get(div + '-mode')
	var z = 0
	if (mode == 'single')
	{
		geo.map_set_marker('bing', window.maps[div], window.markers_stack[working_image], false, _div)
	}
	else
	{
		for (var i in window.markers_stack)
		{
			if (window.markers_stack[i]['start'])
			{
				geo.map_set_marker('bing', window.maps[div], window.markers_stack[i], false, _div)
				geo.id('b1-count').innerHTML = z
				z++
			}
		}
	}
	geo.render_filters()
	return [parseFloat(window.markers_stack[working_image]['lat']), parseFloat(window.markers_stack[working_image]['lng'])]
}

geo.check_tabs = function()
{
	var a, b, c, d
	
	var a = geo.id('switch-tab-1')
	var b = geo.id('switch-tab-2')
	var c = geo.id('file_ops')
	var d = geo.id('project_ops')
	
	if (a.getAttribute('class').indexOf('inactive') != -1)
	{
		c.style.display = 'none'
		d.style.display = 'block'
	}
	else
	{
		d.style.display = 'none'
		c.style.display = 'block'
	}
}

geo.tabChange = function(id, box, other_id)
{
	var a, b, c
	// background: #444;color: #fff;padding: 7px;cursor: pointer
	// border: 1px solid #444;background: #222;border-bottom: 0;color: #fff;padding: 7px;cursor: pointer
	a = geo.id(id + '-data')
	geo.id(box).innerHTML = a.innerHTML
	_other_id = other_id.split(',')
	for (b = 0; b < _other_id.length; b++)
	{
		//geo.id(id).style.background = 'red'
		if (id == _other_id[b])
		{
			c = geo.id(id).setAttribute('class', 'tab tab-active')
		}
		else
		{
			c = geo.id(_other_id[b]).setAttribute('class', 'tab tab-inactive')
		}
	}
	
	// fucking ugly (specific for this app only i think)
	geo.check_tabs()
}

geo.tabInit = function(switch_id, content_box)
{
	var self = this
	var attr
	for (var i in switch_id)
	{
		if (geo.id(switch_id[i]))
		{
			attr = geo.id(switch_id[i])
			if (i == 0) attr.setAttribute('class', 'tab tab-active'); else attr.setAttribute('class', 'tab tab-inactive');
		}
	}
	
	if (typeof switch_id == 'object')
	{
		var a
		
		geo.id('switch-content').innerHTML = geo.id(switch_id[0] + '-data').innerHTML
		
		//geo.id(switch_id[0]).setAttribute('class', 'tab tab-active')
		
		for (var x in switch_id)
		{
			a = geo.id(switch_id[x])
			if (a)
			{
				self.helpers.bindEvent(a, 'click', new Function("geo.tabChange(\'"+switch_id[x]+"\', \'"+content_box+"\', \'"+switch_id+"\')"))
			}
		}
	}
	
	// fucking ugly again
	geo.check_tabs()
}

/**
 * Create new project
 */

geo.project_create = function()
{
	var name = prompt("New project name:")
	if (name != null)
	{
		var desc = prompt("Project description:")
		if (desc != null)
		{
			var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
			yajax.send({
				"method": "POST",
				"url": "/project/create/",
				"data": {"name": encodeURIComponent(name), "desc": encodeURIComponent(desc)},
				"header": headers,
				"success": function(response)
				{
					if (response == '1')
					{
						alert('project is already exist')
					}
					else
					{
						geo.clear_panels()
						//console.debug(response)
						geo.id('project-edit-name').innerHTML = name
						geo.id('project-edit-desc').innerHTML = desc
						
						geo.set('working_image', '')
						geo.set('working_project', name)
						geo.id('project-name').innerHTML = name
						geo.render_files('switch-tab-1-data', {})
						geo.data_request('projects', function(resp) {
							geo.render_projects('switch-tab-2-data', resp)
							geo.tabInit(['switch-tab-1', 'switch-tab-2'], 'switch-content')
						})
					//geo.item_select('switch-tab-2-data', name, 'working_project')
					}
				}
			})
		}
	}
}

// wrap for delete
geo._project_delete = function()
{
	var div = geo.id('switch-content')
	//alert(geo.obj_size(div))
	if (geo.helpers.obj_size(div) > 0)
	{
		for (var i in div.childNodes)
		{
			if (typeof div.childNodes[i] == 'object')
			{
				if (div.childNodes[i].className)
				{
					if (div.childNodes[i].getAttribute('class').indexOf("inactive") == -1)
					{
						geo.project_delete(div.childNodes[i].innerHTML)
					}
				}
			}
		}
	}
}

geo._image_delete = function()
{
	var div = geo.id('switch-content')
	if (geo.helpers.obj_size(div) > 0)
	{
		for (var i in div.childNodes)
		{
			if (typeof div.childNodes[i] == 'object')
			{
				if (div.childNodes[i].className)
				{
					if (div.childNodes[i].getAttribute('class').indexOf("inactive") == -1)
					{
						geo.image_delete(div.childNodes[i].innerHTML, div.childNodes[i].getAttribute('itemId'))
					}
				}
			}
		}
	}
}

geo.image_delete = function(file_name, id)
{
	var a = confirm("Are you sure you want to delete \""+ file_name + "\" ?")
	if (a == true)
	{
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			"method": "POST",
			"url": "/image/delete/",
			"data": {"id": encodeURIComponent(id)},
			"header": headers,
			"success": function(response)
			{
				if (response == '0')
				{
					geo.set('working_image', '')
					geo.get_data()
					//geo.set('working_project', name)
					/*
					geo.data_request('files', function(resp){
						geo.render_files('switch-tab-1-data', resp)
						geo.tabInit(['switch-tab-1', 'switch-tab-2'], 'switch-content')
					})
					*/
				}
				else
				{
					alert(response)
				}
			}
		})
		return true
	}
	else
	{
		return false
	}
}

geo.file_download = function()
{
	var el = document.createElement("iframe")
	el.style.width = '0px'
	el.style.height = '0px'
	el.id = 'iframe'
	document.body.appendChild(el)
	el.src = '/api/download/?file=' + geo.get('working_image')
}

geo.clean_env = function()
{
	geo.set('working_project', '')
	geo.set('working_project', '')
	
	window.env = ''
	window.projects = ''
	window.markers_stack = ''
	window.filters_stack = ''
}

geo.project_delete = function(proj_name, div)
{
	var a = confirm("Are you sure you want to delete \"" + proj_name + "\" ?")
	var prj = proj_name
	
	if (a == true)
	{
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			"method": "POST",
			"url": "/project/delete/",
			"data": {"name": encodeURIComponent(proj_name)},
			"header": headers,
			"success": function(response)
			{
				if (proj_name == geo.get('working_project'))
				{
					geo.clean_env()
					geo.get_data()
				}
				//alert(response)
				window.projects = JSON.parse(response)
				geo.render_projects('switch-tab-2-data', window.projects)
				geo.tabInit(['switch-tab-1', 'switch-tab-2'], 'switch-content')
				//geo.clean_env()
				
				/*
				if (response == '0')
				{
					geo.set('working_image', '')
					geo.set('working_project', name)
					geo.render_files('switch-tab-1-data', {})
					geo.data_request('projects', function(resp){
						geo.render_projects('switch-tab-2-data', resp)
						geo.tabInit(['switch-tab-1', 'switch-tab-2'], 'switch-content')
					})
				}
				else
				{
					alert(response)
				}
				*/
				//geo.item_select('switch-tab-2-data', name, 'working_project')
			}
		})
		return true
		//}
	}
	else
	{
		return false
	}
}

/**
 * Upload handler
 */

geo.handleFileUpload = function(event)
{
	if (window.FormData && window.FileReader)
	{
		files = event.target.files
		
		geo.id('file-loading').style.display = 'block'
		geo.id('file-input').style.display = 'none'
		
		var file = geo.id('file_upload')
		var content = new FormData()
		
		if (file.files[0].size > 6000000)
		{
			alert('file is too heavy (limit 6.000.000 bytes)')
			return
		}
		
		//var _name = encodeURIComponent(file.files[0].name)
		var _name = escape(file.files[0].name)
		var nocache = '?nocache=' + new Date().getTime()
		// 3rd parameter not working in Chrome. is a *BUG*
		content.append('file', file.files[0])
		
		var xhr = new XMLHttpRequest()
		xhr.open('POST', '/api/upload/' + nocache, true)
		xhr.timeout = 3000
		xhr.setRequestHeader("X-CSRFToken", geo.helpers.readCookie('csrftoken'))
		xhr.setRequestHeader("X-Filename", _name)
		xhr.onload = function(e)
		{
			geo.id('file-loading').style.display = 'none'
			geo.id('file-input').style.display = 'block'
			
			if (this.response == '2')
			{
				alert('file already exist')
			}
			else if (this.response == '4')
			{
				alert('You need to create at least one project')
			}
			else
			{
				geo.set('working_image', file.files[0].name)
				geo.get_data('all', geo.render_files)
				//alert('from  upload: ' + file.files[0].name + "\n\nDebug response: " + this.response)
			}
		}
		xhr.ontimeout = function(e)
		{
			alert('request timed out')
		}
		xhr.send(content)
		file.files.length = 0
		content = null
			
			/*
			var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
			yajax.send({
				"method": "POST",
				"url": "/api/upload/",
				"data": content,
				"header": headers,
				"success": function(response) { alert(response) }
			})
			*/
			
			//console.log(f)
			//alert(f.name)
			/*
			var reader = new FileReader()
			reader.onload = function(file)
			{
				console.log(file)
			}
			*/
		//}
	}
	else
	{
		alert('Too old browser. Upload API not working.')
	}
}

/**
 * Search for active thumbnail image
 */

geo.thumb_set_active = function()
{
	var el = geo.id('thumbnails'), img = geo.get('working_image')
	
	if (el.childNodes.length > 0)
	{
		for (var x in el.childNodes)
		{
			if (typeof el.childNodes[x] == 'object')
			{
				if (el.childNodes[x].id)
				{
					if (el.childNodes[x].id == img)
					{
						el.childNodes[x].setAttribute('class', 'thumb img_shadow_active')
					}
					else
					{
						el.childNodes[x].setAttribute('class', 'thumb img_shadow')
					}
				}
			}
		}
	}
}

/**
 * Render thumbnails
 */

geo.render_thumbnails = function(data)
{
	//console.debug(data)
	var out = '', img, div, img_id_stack
	geo.id('thumbnails').innerHTML = ''
	if (geo.helpers.obj_size(data) > 0)
	{
		//console.debug(data)
		window.thumb_stack = []
		window.thumb_stack_names = []
		//alert(geo.helpers.obj_size(data))
		//for (var a in data)
		//console.debug(data)
		for (var a = geo.helpers.obj_size(data); a != 0; a--)
		{
			//alert(a)
			// filters
			if (geo.get('filters') == 'on' && geo.helpers.obj_size(data) > 0)
			{
				var b = data[a - 1]['name'], obj = window.markers_stack[b]
				//console.debug(obj)
				if (!geo.filters_grant_view(obj)) show = false; else show = true;
				append_1_element = true
			}
			else
			{
				show = true
			}
			if (show)
			{
				window.thumb_stack.push(data[a - 1]['id'])
				window.thumb_stack_names.push(data[a - 1]['name'])
			}
		}
		
		//geo.id('thumb_info').innerHTML = 'Total: ' + window.thumb_stack.length
		geo._preload_image()
	}
	else
	{
		out = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px">No files</div>'
	}
	//div.innerHTML = out
}

/**
 * Image step-by-step loading
 */

geo._preload_image = function()
{
	if (window.thumb_stack.length != 0)
	{
		geo.id('thumb_left').innerHTML = 'left to load: ' + window.thumb_stack.length
		var id = window.thumb_stack.pop()
		var name = window.thumb_stack_names.pop()
		geo.preload_image(id, function(){
			
			var div, node, node1, node2, node3
			
			div = geo.id('thumbnails')
			
			node  = document.createElement('div')
			node1 = document.createElement('div')
			node2 = document.createElement('div')
			node3 = document.createElement('input')
			
			if (window.shared_project != '' && window.shared_user_id != '')
			{
				add = '&prj_id=' + window.shared_project + '&user_id=' + window.shared_user_id
			}
			else
			{
				add = ''
			}
			
			if (escape(name) != geo.get('working_image')) node.setAttribute('class', 'thumb img_shadow'); else node.setAttribute('class', 'thumb img_shadow_active');
			node.id = name
			node1.style.background = 'url(/image/thumbnails/?id=' + id + add + ') center center'
			node1.style.width     = '80px'
			node1.style.height    = '80px'
			node1.style.textAlign = 'right'
			
			var z = id
			node1.onclick = function() { geo.image_load(z) }
			
			node2.style.position = 'absolute'
			node2.style.right = '0'
			node2.style.top = '0'
			
			node3.type = 'checkbox'
			node3.style.padding = '0'
			node3.style.margin  = '0'
			
			// image checkbox
			
			//node2.appendChild(node3)
			node.appendChild(node1)
			node.appendChild(node2)
			
			div.appendChild(node)
			geo._preload_image()
		})
	}
	else
	{
		geo.id('thumb_left').innerHTML = ''
	}
}

/**
 * Preload image from thumbnail id's global stack
 */

geo.preload_image = function(id, callback)
{
	var img
	img = new Image()
	img.onload = callback
	
	if (window.shared_project != '' && window.shared_user_id != '')
	{
		add = '&prj_id=' + window.shared_project + '&user_id=' + window.shared_user_id
	}
	else
	{
		add = ''
	}
	
	img.src = '/image/thumbnails/?id=' + id + add
}

geo.data_request = function(type, callback, filters)
{
	var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
	yajax.send({
		"method": "GET",
		"url": "/api/env/",
		"data": {"get": type, "filt": filters, "shared_project": window.shared_project, "shared_user_id": window.shared_user_id},
		"header": headers,
		"nocache": geo.helpers.timestamp(),
		"success": function(response)
		{
			callback(JSON.parse(response))
		}
	})
}

/**
 * kind of init
 */

geo.get_data = function(type)
{
	geo.data_request('all', function(response)
	{
		geo.filters_switch_init()
		window.env = response['env']
		window.projects = response['projects']
		window.markers_stack = geo.calc_additional_params(response['markers'])
		window.filters_stack = response['filters']
		geo.set('working_project', response.env.working_project)
		if (typeof response.env.working_project != 'undefined') geo.id('project-name').innerHTML = response.env.working_project; else geo.id('project-name').innerHTML = '&nbsp;';
		var a = geo.render_files('switch-tab-1-data', response['files'])
		geo.render_projects('switch-tab-2-data', response['projects'])
		geo.tabInit(['switch-tab-1', 'switch-tab-2'], 'switch-content')
		geo.render_thumbnails(response['files'])
		geo.filter.render()
		geo.image_load(a)
		geo.marker_settings()
		geo.checkbox_project_shared(response['env']['is_project_shared'])
		var map1 = geo.get('b1')
		if (!map1 || map1 == 'undefined') map1 = 'google'
		var map2 = geo.get('b2')
		if (!map2 || map2 == 'undefined') map2 = 'yandex'
	})
}

geo.project_select = function(id)
{
	div = geo.id('switch-content')
	prj_name = geo.get('working_project')
	
	for (var item in div.childNodes)
	{
		if (typeof div.childNodes[item] == 'object')
		{
			if (div.childNodes[item].getAttribute('itemid') == id)
			{
				if (typeof div.childNodes[item] == 'object') div.childNodes[item].setAttribute('class', 'file active')
			}
			else
			{
				if (typeof div.childNodes[item] == 'object') div.childNodes[item].setAttribute('class', 'file inactive')
			}
		}
	}
}

geo.clear_panels = function()
{
	geo.id('project-name').innerHTML = ''
	geo.id('prime_image').innerHTML = ''
	geo.id('photo-name').innerHTML = ''
	geo.id('thumbnails').innerHTML = ''
	geo.id('geodata').innerHTML = ''
}

geo.project_load = function(id)
{
	div = geo.id('switch-content')
	prj_name = geo.get('working_project')
	
	for (var item in div.childNodes)
	{
		if (typeof div.childNodes[item] == 'object')
		{
			if (div.childNodes[item].getAttribute('class') == 'file active')
			{
				var id = div.childNodes[item].getAttribute('itemid')
			}
		}
	}
	
	geo.id('project-name').innerHTML = '<img src="/static/img/1.gif">'
	var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
	yajax.send({
		"method": "POST",
		"url": "/project/load/",
		"data": {"id": encodeURI(id)},
		"header": headers,
		"success": function(response)
		{
			if (response != '0')
			{
				var resp = JSON.parse(response)
				window.env = resp['env']
				window.projects = resp['projects']
				window.markers_stack = geo.calc_additional_params(resp['markers'])
				window.filters_stack = resp['filters']
				geo.set('working_image', '')
				geo.set('working_project', resp['env']['working_project'])
				geo.id('project-name').innerHTML = resp['env']['working_project']
				var a = geo.render_files('switch-tab-1-data', resp['files'])
				var b = geo.render_projects('switch-tab-2-data', false)
				geo.tabInit(['switch-tab-1', 'switch-tab-2'], 'switch-content')
				geo.render_thumbnails(resp['files'])
				geo.filter.render()
				geo.image_load(a)
				
				geo.checkbox_project_shared(resp['env']['is_project_shared'])
				
				var map1 = geo.get('b1')
				if (!map1 || map1 == 'undefined') map1 = 'google'
				var map2 = geo.get('b2')
				if (!map2 || map2 == 'undefined') map2 = 'yandex'
				
				geo.change_map('b1', map1)
				geo.change_map('b2', map2)
			}
			else
			{
				alert('server-side: project not found')
			}
		}
	})
}

geo.checkbox_project_shared = function(a)
{
	if (typeof a != 'undefined' && a != false)
	{
		geo.id('project-shared').checked = 'checked'
	}
	else
	{
		geo.id('project-shared').checked = ''
	}
}

geo.checkbox_project_switch = function(el)
{
	var name = escape(geo.get('working_project'))
	var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
	yajax.send({
		"method": "POST",
		"url": "/project/share/",
		"data": {"name": name},
		"header": headers,
		"success": function(response)
		{
			alert(response)
		}
	})
}

/**
 * render projects
 */

geo.render_projects = function(id, data, callback)
{
	var div = geo.id(id)
	var prj_name = geo.get('working_project')
	
	if (window.shared_project == '' && window.shared_user_id == '')
	{
		if (div && typeof data == 'object')
		{
			if (geo.helpers.obj_size(data) > 0)
			{
				out = ''
				
				if (!prj_name) var append_1_element = true
				for (var a in data)
				{
					if (append_1_element)
					{
						cls = 'active'
						append_1_element = false
						geo.set('working_project', a)
					}
					else
					{
						if (a == prj_name)
						{
							cls = 'active'
						}
						else
						{
							cls = 'inactive'
						}
					}
					out += '<div class="file ' + cls + '" desc="' + encodeURIComponent(data[a]['desc']) + '" itemId="' + data[a]['id'] + '" onclick="geo.project_select(this.getAttribute(\'itemId\'))">' + decodeURIComponent(a) + '</div>'
				}
			}
			else
			{
				out = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px">No projects created</div>'
			}
			div.innerHTML = out
		}
		else
		{
			for (var item in div.childNodes)
			{
				if (div.childNodes[item].innerHTML == prj_name)
				{
					if (typeof div.childNodes[item] == 'object') div.childNodes[item].setAttribute('class', 'file active')
				}
				else
				{
					if (typeof div.childNodes[item] == 'object') div.childNodes[item].setAttribute('class', 'file inactive')
				}
			}
		}
		
		if (typeof callback == 'function') callback()
	}
	else
	{
		geo.id('switch-tab-2').parentNode.removeChild(geo.id("switch-tab-2"))
	}
}

geo.render_map = function(div, map, _callback)
{
	var box
	var callback = _callback
	
	box = geo.id(div)
	if (box)
	{
		switch(map)
		{
			case('yandex'):
				geo.loading_progress(div)
				geo.load_yandex(div)
				break
			case('bing'):
				geo.loading_progress(div)
				geo.load_bing(div)
				break
			case('google'):
				geo.loading_progress(div)
				geo.load_google(div)
				break
			case('osm'):
				geo.loading_progress(div)
				geo.load_osm(div)
				break
		}
	}
	else
	{
		alert('no map div created')
	}
}

/* TODO: merge with render_map() */

geo.change_map = function(block, id, callback)
{
	switch (block)
	{
		case('b1'):
			geo.set('b1', id)
			geo.widget_edit_close('b1')
			break
		case('b2'):
			//if (!id) var id = 'yandex'
			geo.set('b2', id)
			geo.widget_edit_close('b2')
			break
	}
	
	// set active button
	var arr = ['yandex', 'google', 'osm', 'bing']
	for (var a in arr)
	{
		geo.id(arr[a] + '-' + block).setAttribute('class', 'tsc_c3b_small tsc_button')
	}
	geo.id(id + '-' + block).setAttribute('class', 'tsc_c3b_small tsc_button tsc_c3b_green')
	
	var el = geo.id(block)
	el.innerHTML = ''
	if (el)
	{
		switch(id)
		{
			case('yandex'):
				geo.loading_progress(id)
				var latlng = geo.load_yandex(block)
				break
			case('bing'):
				geo.loading_progress(id)
				var latlng = geo.load_bing(block)
				break
			case('google'):
				geo.loading_progress(id)
				var latlng = geo.load_google(block)
				break
			case('osm'):
				geo.loading_progress(id)
				var latlng = geo.load_osm(block)
				break
		}
		if (latlng)
		{
			geo.get_address(latlng)
		}
		else
		{
			geo.id('address').innerHTML = 'No geo data'
			geo.id('lat').innerHTML = ''
			geo.id('lng').innerHTML = ''
		}
	}
	else
	{
		alert('No place for map')
	}
}

geo.widget_edit_close = function(id)
{
	//alert(id)
	var div = geo.id(id + '-pre')
	var a   = geo.id(id + '-marker-mode-button')
	var b   = geo.id('edit-mode-button')
	if (a && div)
	{
		//alert(typeof b)
		if (id == 'b1')
		{
			if (b) div.removeChild(b)
		}
		div.removeChild(a)
	}
}

/**
 * Map move to center
 */

geo.map_to_center = function(id)
{
	var map = geo.get(id)
	if (map)
	{
		var lat = window.maps[id]['current_marker'][0]
		var lng = window.maps[id]['current_marker'][1]
		switch(map)
		{
			case('google'):
				
				window.maps[id].setCenter(new google.maps.LatLng(lat, lng))
				
				break
				
			case('yandex'):
				
				window.maps[id].setCenter([lat, lng])
				
				break
				
			case('osm'):
				
				window.maps[id].setCenter(new OpenLayers.LonLat(lng, lat).transform(new OpenLayers.Projection("EPSG:4326"), window.maps[id].getProjectionObject()))
				
				break
				
			case('bing'):
				
				window.maps[id].setView({center: new Microsoft.Maps.Location(lat, lng)})
				
				break
		}
	}
}

/**
 * Init marker edit mode
 */

geo.init_marker_mode = function()
{
	var img = new Image()
	img.onload = function()
	{
		geo.id('edit-mode-button').parentNode.removeChild(geo.id('edit-mode-button'))
		geo.id('b1-marker-mode-button').parentNode.removeChild(geo.id('b1-marker-mode-button'))
		
		var el = geo.id('b1-pre'), w, h, a, b, c
		if (el)
		{
			geo.map_to_center('b1')
			w = (el.offsetWidth / 2) - (128 / 2)
			h = (el.offsetHeight / 2) - (128 / 2)
			a = document.createElement('img')
			a.style.position = 'absolute'
			a.style.left = w + 'px'
			a.style.top = h + 'px'
			a.src = '/static/img/crosshair.png'
			a.id = 'crosshair-img'
			el.appendChild(a)
			
			// "apply" button
			b = document.createElement('div')
			b.style.position = 'absolute'
			b.style.left = '10px'
			b.style.bottom = '35px'
			b.style.width = '200px'
			b.innerHTML = '<div class="button green" id="save-position" style="padding: 5px 10px;display: inline-block;width: 100px" onclick="geo.save_marker_position(true)">save position</div><div class="button red" style="padding: 5px 10px;display: inline-block" onclick="geo.clear_widgets();geo.widget_edit_render();">cancel</div>'
			b.id = 'save-position-btn'
			//b.onclick = 
			
			c = document.createElement('div')
			c.style.position = 'absolute'
			c.style.bottom = '0px'
			c.style.left = '0px'
			c.style.width = '100%'
			//c.style.height = '50px'
			c.id = 'angle-maker'
			c.innerHTML = '<div id="simple-slider" class="dragdealer rounded-cornered"><div class="red-bar handle"><span id="angle">0</span></div></div>'
			
			var l = document.createElement('div')
			l.style.position = 'absolute'
			l.id = 'marker-loading'
			l.style.bottom = '10px'
			l.style.left = '10px'
			l.style.width = '100px'
			l.style.textAlign = 'center'
			l.style.background = '#202020'
			l.innerHTML = '<img src="/static/img/1.gif">'
			el.appendChild(l)
			
			geo.load_script("http://"+window.location.hostname+'/static/js/dragdealer.js?nocache=' + geo.helpers.timestamp(), function(i){
				
				if (typeof Dragdealer == 'function')
				{
					geo.id('marker-loading').parentNode.removeChild(geo.id('marker-loading'))
					
					el.appendChild(c)
					el.appendChild(b)
					
					clearInterval(i)
					
					function getAngle(ctx, x, y, angle, h)
					{
						var radians = angle * (Math.PI / 180)
						return { x: x + h * Math.cos(radians), y: y + h * Math.sin(radians) }
					}
					
					function changeLine(angle)
					{
						var canvas = geo.id('canvas-azimuth')
						if (!canvas)
						{
							//alert('touch')
							var d = document.createElement('canvas')
							d.id = 'canvas-azimuth'
							d.style.width = '100px'
							d.style.height = '100px'
							d.style.position = 'absolute'
							var el = geo.id('b1-pre')
							var w = (el.offsetWidth / 2) - (100 / 2)
							var h = (el.offsetHeight / 2) - (100 / 2)
							d.style.left = w + 'px'
							d.style.top = h + 'px'
							
							el.appendChild(d)
						}
						else
						{
							d = canvas
						}
						
						if (typeof G_vmlCanvasManager != 'undefined')
						{ 
							G_vmlCanvasManager.initElement(d)
						}
						
						var ctx = d.getContext('2d')
						d.width = 100
						d.height = 100
						ctx.clearRect (0 , 0 , 100 , 100)
						ctx.beginPath()
						ctx.lineWidth = 5
						ctx.moveTo(50, 50)
						var pos = getAngle(ctx, 50, 50, angle + 270, 50)
						ctx.lineTo(pos.x, pos.y)
						ctx.stroke()
					}
					
					var dd = new Dragdealer('simple-slider', {
						steps: 360,
						snap: true,
						loose: true,
						animationCallback: function(x, y)
						{
							var angle = Math.round(x * (360 - 1)) + 1
							geo.id('angle').innerHTML = angle
							changeLine(angle)
						}
					})
					
					var working_image = escape(geo.get('working_image'))
					dd.setStep(window.markers_stack[working_image]['az'])
				}
				
			})
			
		}
	}
	img.src = '/static/img/crosshair.png'
}

geo.clear_widgets = function()
{
	var a = geo.id('save-position-btn')
	if (a) a.parentNode.removeChild(a)
	var b = geo.id('crosshair-img')
	if (b) b.parentNode.removeChild(b)
	var c = geo.id('simple-slider')
	if (c) c.parentNode.removeChild(c)
	var d = geo.id('canvas-azimuth')
	if (d) d.parentNode.removeChild(d)
}

/**
 * Save marker position
 */

geo.save_marker_position = function(flag)
{
	if (flag)
	{
		var id = geo.get('b1')
		var az  = geo.id('angle').innerHTML
		geo.id('save-position').innerHTML = '<img src="/static/img/1.gif">'
		
		switch(id)
		{
			case('google'):
				
				var center = window.maps['b1'].getCenter()
				var lat = center.lat()
				var lng = center.lng()
				
				break
				
			case('yandex'):
				
				break
				
			case('bing'):
				
				var center = window.maps['b1'].getCenter()
				//console.debug(center)
				var lat = center.latitude
				var lng = center.longitude
				break
				
			case('osm'):
				
				var center = window.maps['b1'].getCenter().transform(new OpenLayers.Projection("EPSG:900913"), new OpenLayers.Projection("EPSG:4326"))
				//console.debug(center)
				var lat = center.lat
				var lng = center.lon
				break
		}
		
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			"method": "POST",
			"url": "/image/write/data/",
			"data": {"lat": lat, "lng": lng, "az": az, "name": escape(geo.get('working_image'))},
			"header": headers,
			"success": function(response)
			{
				geo.data_request('all', function(resp){
					window.markers_stack = geo.calc_additional_params(resp['markers'])
					geo.clear_widgets()
					geo.change_map('b1', geo.get('b1'))
				})
				//geo.item_select('switch-tab-2-data', name, 'working_project')
			}
		})
	}
}

/**
 * Marker mode button
 */

geo.widget_edit_render = function()
{
	var div  = geo.id('b1-pre')
	var div1 = geo.id('b2-pre')
	
	if (!geo.id('edit-mode-button'))
	{
		var a = document.createElement('div')
		a.id = 'edit-mode-button'
		a.style.position = 'absolute'
		a.style.top = '0px'
		a.style.left = '0px'
		a.style.padding = '10px'
		a.onclick = geo.init_marker_mode
		//a.style.background = 'url(/static/img/tr.png)'
		a.innerHTML = '<span class="minimal" style="margin: 0;padding: 5px 10px">Edit marker</span>'
		a.style.color = '#fff'
		div.appendChild(a)
	}
	
	if (!geo.id('b1-marker-mode-button'))
	{
		var b = document.createElement('div')
		b.id = 'b1-marker-mode-button'
		b.style.position = 'absolute'
		b.style.bottom= '0px'
		b.style.left = '0px'
		b.style.padding = '10px'
		b.style.margin = '10px'
		b.style.border = '1px solid #ccc'
		//b.style.background = '#fff'
		b.style.cursor = 'pointer'
		b.onclick = function() { geo.marker_change_mode('b1') }
		b.innerHTML = '<img src="/static/img/menu_marker_settings.png">'
		b.zIndex = '5000'
		b.style.color = '#fff'
		div.appendChild(b)
	}
	
	if (!geo.id('b2-marker-mode-button'))
	{
		var c = document.createElement('div')
		c.id = 'b2-marker-mode-button'
		c.style.position = 'absolute'
		c.style.bottom= '0px'
		c.style.left = '0px'
		c.style.padding = '10px'
		c.style.margin = '10px'
		c.style.border = '1px solid #ccc'
		//b.style.background = '#fff'
		c.style.cursor = 'pointer'
		c.onclick = function() { geo.marker_change_mode('b2') }
		c.innerHTML = '<img src="/static/img/menu_marker_settings.png">'
		c.zIndex = '5000'
		c.style.color = '#fff'
		div1.appendChild(c)
	}
	
	geo.switch_button('b1')
	geo.switch_button('b2')
}

/**
 * Switch
 */

geo.switch_button = function(id)
{
	var b = geo.id(id + '-marker-mode-button')
	var mode = geo.get(id + '-marker-mode-advanced')
	if (!mode || mode == 'undefined')
	{
		geo.set(id + '-marker-mode-advanced', 'off')
		b.setAttribute('switched', 'off')
		b.style.background = '#E8E8E8'
	}
	else
	{
		if (mode == 'on')
		{
			b.style.background = '#fff'
			b.setAttribute('switched', 'on')
		}
		else
		{
			b.style.background = '#E8E8E8'
			b.setAttribute('switched', 'off')
		}
	}
}

/**
 * Switch marker: simple or advanced
 */

geo.marker_change_mode = function(id)
{
	var btn = geo.id(id + '-marker-mode-button')
	if (btn.getAttribute('switched') == 'off')
	{
		btn.setAttribute('switched', 'on')
		geo.set(id + '-marker-mode-advanced', 'on')
		btn.style.background = '#fff'
	}
	else
	{
		btn.setAttribute('switched', 'off')
		geo.set(id + '-marker-mode-advanced', 'off')
		btn.style.background = '#E8E8E8'
	}
	
	var map = geo.get(id)
	geo.change_map(id, map)
	
	//alert(btn.getAttribute('switched'))
}

geo.loading_progress = function(div)
{
	var a, b
	
	a = geo.id(div)
	if (a)
	{
		b = document.createElement('div')
		b.style.position = 'absolute'
		b.style.padding = '15px'
		b.style.background = 'brown'
		b.style.color = '#fff'
		b.innerHTML = 'Loading ...'
		b.textAlign = 'center'
		b.style.top = '0'
		b.style.right = '0'
		b.zIndex = 3000
		a.appendChild(b)
	}
}

geo.render_files = function(id, data, callback)
{
	var div, out, img_name, append_1_element, flag_image_load, show, obj, b
	
	div = geo.id(id)
	//console.debug(data)
	if (div && typeof data == 'object')
	{
		if (geo.helpers.obj_size(data) > 0)
		{
			out = ''
			img_name = geo.get('working_image')
			if (!img_name || img_name == '' || !window.markers_stack[img_name]) append_1_element = true
			
			window.files_stack = data
			for (var a in data)
			{
				// filters
				if (geo.get('filters') == 'on' && geo.helpers.obj_size(data) > 0)
				{
					var b = data[a]['name'], obj = window.markers_stack[escape(b)]
					//console.debug(window.markers_stack)
					if (!geo.filters_grant_view(obj)) show = false; else show = true;
					append_1_element = true
				}
				else
				{
					show = true
				}
				
				if (show)
				{
					if (append_1_element)
					{
						cls = 'active'
						append_1_element = false
						geo.set('working_image', data[a]['name'])
						flag_image_load = data[a]['id']
						window.current_file_id = flag_image_load
					}
					else
					{
						if (data[a]['name'] == img_name)
						{
							flag_image_load = data[a]['id']
							window.current_file_id = flag_image_load
							// next | prev images
							if (a == 0)
							{
								window.prev = 0
								window.next = 1
							}
							else
							{
								
							}
							
							cls = 'active'
						}
						else
						{
							cls = 'inactive'
						}
					}
					
					out += '<div class="file ' + cls + '" itemId="' + data[a]['id'] + '" onclick="geo.image_load(this.getAttribute(\'itemId\', this.innerHTML))">' + unescape(data[a]['name']) + '</div>'
				}
			}
			if (out.length < 1) out = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px">No files</div>'
		}
		else
		{
			out = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px">No files uploaded</div>'
		}
		
		div.innerHTML = out
	}
	else
	{
		div.innerHTML = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px">No files uploaded</div>'
		
		/*
		for (var item in div.childNodes)
		{
			if (div.childNodes[item].innerHTML == name)
			{
				if (typeof div.childNodes[item]) div.childNodes[item].setAttribute('class', 'file active')
			}
			else
			{
				if (typeof div.childNodes[item]) div.childNodes[item].setAttribute('class', 'file inactive')
			}
		}
		*/
	}
	
	if (typeof callback == 'function') callback(flag_image_load)
	
	return flag_image_load
}

geo.loading_central = {

	start: function()
	{
		var a, el
		
		a = geo.id('top-panel')
		el = document.createElement('div')
		el.style.position = 'absolute'
		el.style.top = '0'
		el.style.right = '10px'
		el.style.padding = '20px'
		el.style.background = 'green'
		el.id = 'loading-central'
		el.innerHTML = '<img src="/static/img/1.gif">'
		a.appendChild(el)
	},
	stop: function()
	{
		if (geo.id('loading-central'))
		{
			geo.id('loading-central').parentNode.removeChild(geo.id('loading-central'))
		}
	},
	is_active: function()
	{
		if (geo.id('loading-central')) return true; else return false;
	}
}

/**
 * Load prime image
 */

geo.image_load = function(id)
{
	if (typeof id != 'undefined')
	{
		var opts = geo.spin
		
		if (!geo.loading_central.is_active()) geo.loading_central.start()
		var img = geo.id('central_image')
		
		function cached(url)
		{
			var test = document.createElement("img")
			test.src = url
			return test.complete || test.width+test.height > 0
		}
		
		img.onload = function()
		{
			geo.loading_central.stop()
		}
		
		img.onerror = function()
		{
			geo.loading_central.stop()
		}
		
		if (window.shared_project != '' && window.shared_user_id != '')
		{
			add = '?prj_id=' + window.shared_project + '&user_id=' + window.shared_user_id
		}
		else
		{
			add = ''
		}
		
		img.src = '/image/get/'+ id + '.jpg' + add
		window.current_file_id = id
		geo.render_image_data(id)
		var item_name = geo.item_select(id)
		geo.id('photo-name').innerHTML = item_name
		geo.set('working_image', item_name)
		
		var map1 = geo.get('b1')
		if (!map1 || map1 == null || map1 == 'undefined') map1 = 'google'
		var map2 = geo.get('b2')
		if (!map2 || map2 == null || map2 == 'undefined') map2 = 'yandex'
		
		geo.clear_widgets()
		
		geo.change_map('b1', map1)
		geo.change_map('b2', map2)
		
		geo.thumb_set_active()
		
		// next or prev image id to load
		
		var next, prev
		
		for (var i in window.files_stack)
		{
			if (id == window.files_stack[i]['id'])
			{
				var q
				if (i == 0)
				{
					window.prev = false
					q = parseInt(i) + 1
					if (window.files_stack[q])
					{
						window.next = window.files_stack[q]['id']
					}
				}
				else if (i == (geo.helpers.obj_size(window.files_stack) - 1))
				{
					window.next = false
					q = parseInt(i) - 1
					if (window.files_stack[q])
					{
						window.prev = window.files_stack[q]['id']
					}
				}
				else
				{
					q = parseInt(i) - 1
					window.prev = window.files_stack[q]['id']
					q = parseInt(i) + 1
					window.next = window.files_stack[q]['id']
				}
				break
			}
		}
	}
}

geo.item_select = function(id)
{
	var a
	
	var div = geo.id('switch-content')
	if (geo.helpers.obj_size(div.childNodes) > 0 && typeof div == 'object')
	{
		for (var x in div.childNodes)
		{
			a = div.childNodes[x]
			if (typeof a == 'object')
			{
				if (a.getAttribute('itemId') == id)
				{
					a.setAttribute('class', 'file active')
					out = a.innerHTML
				}
				else
				{
					a.setAttribute('class', 'file inactive')
				}
			}
		}
	}
	return out
}

geo.render_image_data = function(id)
{
	var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
	var self = this
	
	if (window.shared_project != '' && window.shared_user_id != '')
	{
		data = {"id": id, "prj_id": window.shared_project, "user_id": window.shared_user_id}
	}
	else
	{
		data = {"id": id}
	}
	
	yajax.send({
		"method": "GET",
		"url": "/image/data/",
		//"data": {"id": id, "nocache": geo.helpers.timestamp()},
		"data": data,
		"header": headers,
		"success": function(response)
		{
			var a = JSON.parse(response)
			var div = geo.id('geodata')
			var out = ''
			if (typeof a == 'object')
			{
				out += '<table style="float: left">'
				var gap = Math.round(geo.helpers.obj_size(a) / 2)
				var i = 0
				for (var x in a)
				{
					out += '<tr><td><div style="background: #202020;padding: 3px 5px;text-align: right;margin-bottom: 1px">' + x + '</div></td><td><div style="background: #333;padding: 3px 5px;text-align: center;margin-bottom: 1px;font-family: courier;word-wrap: break-word;width: 100px">' + a[x] + '&nbsp;</div></td></tr>'
					
					if (i == gap) out += '</table><table>'
					i++
				}
				out += '</table>'
				if (a.Comments || a.Comments != '') cmnt = a.Comments; else cmnt = 'No comments';
				geo.id('photo-comments').innerHTML = cmnt
				geo.resize()
			}
			else
			{
				geo.id('photo-comments').innerHTML = 'No comments'
				out += '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px;font-size: 16px">No geo data</div>'
				geo.resize()
			}
			div.innerHTML = out
		}
	})
}

/**
 * Reverse geocoding. Address lookup from lat lng.
 */

geo.get_address = function(latlng)
{
	var lat = parseFloat(latlng[0])
	var lng = parseFloat(latlng[1])
	var latlng = new google.maps.LatLng(lat, lng)
	var geocoder = new google.maps.Geocoder()
	geocoder.geocode({'latLng': latlng}, function(results, status)
	{
		if (status == google.maps.GeocoderStatus.OK)
		{
			if (results[1])
			{
				geo.id('address').innerHTML = results[1].formatted_address
				geo.id('lat').innerHTML = ' (<b>lat</b>: '+lat+' , '
				geo.id('lng').innerHTML = '<b>lng</b>: '+lng+')'
			}
		}
		else
		{
			geo.id('address').innerHTML = status
		}
	})
}

/**
 * get "global" variables stored locally
 */

geo.get = function(variable)
{
	var a
	
	if (typeof(Storage) !== "undefined")
	{
		// localStorage and sessionStorage support
		a = localStorage[variable]
		if (a) return a; else return false;
	}
	else
	{
		// using cookie storage
		a = geo.helpers.readCookie(variable)
		if (a) return a; else return false;
	}
}

/**
 * set "global" variables stored locally
 */

geo.set = function(variable, value)
{
	var a
	
	if (typeof(Storage) !== "undefined")
	{
		// localStorage and sessionStorage support
		localStorage[variable] = value
		if (localStorage[variable]) return true; else return false;
	}
	else
	{
		// using cookie storage
		geo.helpers.setCookie(variable, value)
	}
}

/**
 * Why is the google wont make lazy-loading support for their scripts ?
 */

geo.load_google = function(block)
{
	/*
	if (!window.special_for_google) window.special_for_google = []
	window.special_for_google.push(block)
	var callback = callback
	*/
	/*
	if (typeof google != 'object')
	{
		geo.load_script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry&callback=geo.map_render_google', function() {  })
	}
	else
	{
		if (typeof google.maps == 'object')
		{
			geo.map_render_google(block)
		}
		else
		{
			geo.load_script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry&callback=geo.map_render_google', function() {  })
		}
	}
	*/
	
	return geo.map_render_google(block)
	
	/*
	if (window.google_map_loaded)
	{
		geo.map_render_google()
	}
	else
	{
		geo.load_script('https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false&libraries=geometry&callback=geo.map_render_google', function() {  })
	}
	*/
}

geo.load_bing = function(block, callback)
{
	var map_div = block
	if (typeof Microsoft != 'object')
	{
		geo.load_script('https://ecn.dev.virtualearth.net/mapcontrol/mapcontrol.ashx?v=7.0&mkt=ru-RU', function(i){
			
			if (typeof Microsoft == 'object')
			{
				if (typeof Microsoft.Maps == 'object')
				{
					if (typeof Microsoft.Maps.Map == 'function')
					{
						clearInterval(i)
						return geo.map_render_bing(map_div)
					}
				}
			}
		})
	}
	else
	{
		geo.map_render_bing(map_div)
	}
}

geo.load_osm = function(block, callback)
{
	var map_div = block
	
	if (typeof Openlayers != 'object')
	{
		this.load_script("http://"+window.location.hostname + window.static_url + 'js/openlayers.js', function(i){
			
			if (typeof OpenLayers == 'object')
			{
				clearInterval(i)
				return geo.map_render_osm(map_div)
			}
		})
	}
	else
	{
		geo.map_render_osm(map_div)
	}
}

geo.comments_edit = 
{
	comment: null,
	start: function()
	{
		var comment, a, b, z
		this.comment = geo.id('photo-comments').innerHTML
		
		if (this.comment != '')
		{
			if (this.comment == 'No comments')
			{
				z = ''
			}
			else
			{
				z = this.comment
			}
			geo.id('photo-edit-comments').innerHTML = '<textarea style="width: 100%;background: #333;border: 1px solid #000;color: #fff" rows="2" id="cmt">' + z + '</textarea><div class="button green" style="display: inline-block;padding: 3px 15px;margin: 0;margin-right: 10px"  onclick="geo.comments_edit.save()">OK</div><div class="button red" style="display: inline-block;padding: 3px 15px;margin: 0" onclick="geo.comments_edit.stop()">Cancel</div>'
			geo.id('photo-edit-comments').style.display = 'block'
			geo.id('photo-comments').style.display = 'none'
			geo.id('cmt').focus()
		}
	},
	stop: function()
	{
		geo.id('photo-edit-comments').style.display = 'none'
		geo.id('photo-comments').style.display = 'block'
	},
	save: function()
	{
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		var self = this
		yajax.send({
			"method": "POST",
			"url": "/comment/save/",
			"data": {"comment": encodeURIComponent(geo.id('cmt').value), "id": window.current_file_id},
			"header": headers,
			"success": function(response)
			{
				self.stop()
				geo.id('photo-comments').innerHTML = geo.id('cmt').value
			}
		})
	}
}

geo.load_yandex = function(block, callback)
{
	var map_div = block
	
	if (typeof ymaps == 'object' && typeof ymaps.Map == 'function')
	{
		geo.map_render_yandex(map_div)
	}
	else
	{
		this.load_script('http://api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU', function(i){
			
			if (typeof ymaps == 'object')
			{
				if (typeof ymaps.Map == 'function')
				{
					clearInterval(i)
					return geo.map_render_yandex(map_div)
				}
			}
		})
	}
}

geo.scrollbars_init = function()
{
	geo.load_script("http://"+window.location.hostname+'/static/js/dragdealer.js?nocache=' + geo.helpers.timestamp(), function(i){
		
		if (typeof Dragdealer == 'function')
		{
			clearInterval(i)
			
			var mask = geo.id('geodata')
			var content = geo.id('geodata')
			new Dragdealer('geodata',
			{
				horizontal: false,
				vertical: true,
				yPrecision: content.offsetHeight,
				animationCallback: function(x, y)
				{
					var margin = y * (content.offsetHeight - mask.offsetHeight)
					content.style.marginTop = String(-margin) + 'px'
				}
			})
		}
	})
}

geo.filter_switch = function(name)
{
	if (name)
	{
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			"method": "GET",
			"url": '/filter/switch/',
			"data": {"name": encodeURIComponent(name)},
			"header": headers,
			"success": function(response)
			{
				window.filters_stack = JSON.parse(response)
				if (geo.get('filters') == 'on') geo.get_data()
			}
		})
	}
}

/**
 * Filter actions
 */

geo.filter = {
	
	send: function(url, data, callback)
	{
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			"method": "POST",
			"url": url,
			"data": data,
			"header": headers,
			"success": function(response)
			{
				if (typeof callback == 'function') callback(response)
			}
		})
	},
	create: function()
	{
		this.edit_start()
	},
	render: function()
	{
		var el, add
		
		el = geo.id('filters')
		
		out = ''
		if (geo.helpers.obj_size(window.filters_stack) > 0)
		{
			for (var x in window.filters_stack)
			{
				if (window.filters_stack[x]['geodata']['enabled'] === true) add = ' checked="checked"'; else add = ''
				out += '<div style="position: relative"><div class="filt f-inactive" style="padding: 2px" onclick="geo.filter.select(this)">' + x + '</div><div style="position: absolute;right: 0;top: 0"><input onclick="geo.filter_switch(\'' + x + '\')" type="checkbox"' + add + '></div></div>'
				//out += '<div style="position: relative"><div class="filt f-inactive" style="padding: 2px" onclick="geo.filter.select(this)">' + x + '</div></div>'
			}
			el.innerHTML = out
		}
		else
		{
			el.innerHTML = '<div style="background: #2D4B6E;margin: 0px;border: 1px solid #333;padding: 10px">No filters</div>'
		}
	},
	del: function()
	{
		var el = geo.id('filters')
		var _out = []
		for (var x in el.childNodes)
		{
			if (typeof el.childNodes[x] == 'object')
			{
				if (el.childNodes[x].childNodes[0].getAttribute('class') == 'filt f-active')
				{
					_out.push(encodeURIComponent(el.childNodes[x].childNodes[0].innerHTML))
				}
			}
		}
		
		if (_out.length > 0)
		{
			var out = JSON.stringify(_out)
			if (confirm('Are you sure ??'))
			{
				var self = this
				this.send('/filter/delete/?nocache=' + geo.helpers.timestamp(), {"data": out}, function(response){
					window.filters_stack = JSON.parse(response)
					self.render()
					geo.render_filters()
					geo.change_map('b1', geo.get('b1'))
				})
			}
		}
	},
	select: function(node)
	{
		// first unselect all
		/*
		var el = geo.id('filters')
		for (var x in el.childNodes)
		{
			//alert(typeof el.childNodes[x])
			if (typeof el.childNodes[x] == 'object')
			{
				el.childNodes[x].childNodes[0].setAttribute('class', 'file inactive')
			}
		}
		*/
		
		var a = node.getAttribute('class')
		if (a == 'filt f-active') node.setAttribute('class', 'filt f-inactive'); else node.setAttribute('class', 'filt f-active');
	},
	edit: function()
	{
		var a = this.count_active()
		//console.debug(a)
		if (a.length == 1)
		{
			this.edit_start(a[0])
		}
		else if (a.length > 1)
		{
			alert('only one filter to edit')
		}
		else
		{
			alert('select filter to edit')
		}
	},
	count_active: function()
	{
		var el = geo.id('filters')
		var _out = []
		for (var x in el.childNodes)
		{
			if (typeof el.childNodes[x] == 'object')
			{
				if (el.childNodes[x].childNodes[0].getAttribute('class') == 'filt f-active')
				{
					_out.push(encodeURIComponent(el.childNodes[x].childNodes[0].innerHTML))
				}
			}
		}
		
		return _out
	},
	edit_start: function(edit_filter)
	{
		if (typeof window.maps['b1'] != 'object') return
		
		var add, el, a, b
		
		geo.widget_edit_close('b1')
		
		el = geo.id('b1')
		a = document.createElement('canvas')
		a.style.position = 'absolute'
		
		a.style.left = '0'
		a.style.top = '0'
		a.style.width = (el.offsetWidth - 20) + 'px'
		a.style.height = (el.offsetHeight - 20) + 'px'
		a.width = (el.offsetWidth - 20)
		a.height = (el.offsetHeight - 20)
		a.style.border = '10px dotted #303030'
		a.style.cursor = 'crosshair'
		a.style.zIndex = '5000'
		a.id = 'canvas-frame'
		
		if (edit_filter) add = "'" + edit_filter + "'"; else add = '';
		
		// "apply" button
		b = document.createElement('div')
		b.style.position = 'absolute'
		b.style.left = '20px'
		b.style.bottom = '35px'
		//b.style.width = '100%'
		b.style.zIndex = '5001'
		b.innerHTML = '<div class="button green" id="save-position" style="padding: 10px 10px;display: inline-block;" onclick="geo.filter.edit_save(' + add + ')">save</div><div class="button red" style="padding: 10px 10px;display: inline-block" onclick="geo.filter.edit_stop()">cancel</div><div><div style="display: inline-block;background: #f0f0f0;padding: 0 3px;margin-right: 5px;font-size: 11px">Date start: <br><input type="text" id="date-pick-1" style="width: 100px;"><br>Date end: <br><input type="text" id="date-pick-2" style="width: 100px;"></div></div>'
		b.id = 'edit-filter-controls'
		el.appendChild(b)
		
		el.appendChild(a)
		a.setAttribute('class', 'non-select')
		a.setAttribute('unselectable', 'on')
		var self = this
		
		// load external drawing script
		/*
		if (typeof tools != 'object')
		{
			geo.load_script("http://"+window.location.hostname+'/static/js/drawing.js', function(i){
				
				if (typeof tools == 'object')
				{
					clearInterval(i)
					init()
					datepicker()
				}
			})
		}
		else
		{
			init()
			datepicker()
		}
		
		function datepicker()
		{
			geo.load_script("http://"+window.location.hostname+'/static/js/pikaday.js', function(i){
				
				if (typeof window.Pikaday == 'function')
				{
					clearInterval(i)
					var picker1 = new Pikaday({ field: document.getElementById('date-pick-1'), format: "YYYY-MM-DD" })
					var picker2 = new Pikaday({ field: document.getElementById('date-pick-2'), format: "YYYY-MM-DD" })
				}
			})
		}
		*/
		//el.style.border = '10px dashed #fff'
	},
	edit_stop: function()
	{
		geo.id('canvas-frame').parentNode.removeChild(geo.id('canvas-frame'))
		geo.id('edit-filter-controls').parentNode.removeChild(geo.id('edit-filter-controls'))
	},
	edit_save: function(filter_name)
	{
		var pnt1, pnt2, data, se, sw, ne, nw, proj, fname, _x, _x1, _y, _y1
		
		if (filter_name) fname = filter_name
		
		window.data = {}
		
		if (window.y > window.y1)
		{
			_y = window.y1
			_y1 = window.y
			_x = window.x1
			_x1 = window.x
		}
		else
		{
			_y = window.y
			_y1 = window.y1
			_x = window.x
			_x1 = window.x1
		}
	
		switch(geo.get('b1'))
		{
			case('google'):
				
				var ov = new google.maps.OverlayView()
				
				MyOverlay.prototype = new google.maps.OverlayView()
				MyOverlay.prototype.onAdd = function()
				{
					var proj = this.getProjection()
					
					if (window.y > window.y1)
					{
						_y = window.y1
						_y1 = window.y
						_x = window.x1
						_x1 = window.x
					}
					else
					{
						_y = window.y
						_y1 = window.y1
						_x = window.x
						_x1 = window.x1
					}
					
					pnt1 = proj.fromContainerPixelToLatLng(new google.maps.Point(_x, _y))
					pnt2 = proj.fromContainerPixelToLatLng(new google.maps.Point(_x1, _y1))
					
					se = proj.fromContainerPixelToLatLng(new google.maps.Point(_x, _y))
					
					sw = proj.fromContainerPixelToLatLng(new google.maps.Point(_x, _y1))
					
					ne = proj.fromContainerPixelToLatLng(new google.maps.Point(_x1, _y))
					
					nw = proj.fromContainerPixelToLatLng(new google.maps.Point(_x1, _y1))
					
					window.data = {"se": [se.lat(), se.lng()], "sw": [sw.lat(), sw.lng()], "ne": [ne.lat(), ne.lng()], "nw": [nw.lat(), nw.lng()]}
				}
				
				MyOverlay.prototype.onRemove = function() { }
				MyOverlay.prototype.draw = function() { }
				function MyOverlay(map) { this.setMap(map) }
				var overlay = new MyOverlay(window.maps['b1'])
				
				break
				
			case('yandex'):
				
				alert('неправильно пока считает координаты. попробуй с другой карты')
				return
				
				var w = geo.id('b1-pre').offsetWidth / 2
				var h = geo.id('b1-pre').offsetHeight / 2
				
				var proj = window.maps['b1'].options.get('projection')
				
				var pnt1 = proj.fromGlobalPixels(window.maps['b1'].converter.pageToGlobal([window.x, window.y]), window.maps['b1'].getZoom())
				var pnt2 = proj.fromGlobalPixels(window.maps['b1'].converter.pageToGlobal([window.x1, window.y1]), window.maps['b1'].getZoom())
				
				se = proj.fromGlobalPixels(window.maps['b1'].converter.pageToGlobal([window.x, window.y]), window.maps['b1'].getZoom())
				sw = proj.fromGlobalPixels(window.maps['b1'].converter.pageToGlobal([window.x, window.y1]), window.maps['b1'].getZoom())
				ne = proj.fromGlobalPixels(window.maps['b1'].converter.pageToGlobal([window.x1, window.y]), window.maps['b1'].getZoom())
				nw = proj.fromGlobalPixels(window.maps['b1'].converter.pageToGlobal([window.x1, window.y1]), window.maps['b1'].getZoom())
				
				window.data = {"se": [se[0], se[1]], "sw": [sw[0], sw[1]], "ne": [ne[0], ne[1]], "nw": [nw[0], nw[1]]}
				
				break
				
			case('bing'):
				
				// bing center coords workaround
				var w = geo.id('b1-pre').offsetWidth / 2
				var h = geo.id('b1-pre').offsetHeight / 2
				
				var pnt1 = window.maps['b1'].tryPixelToLocation(new Microsoft.Maps.Point(_x, _y))
				var pnt2 = window.maps['b1'].tryPixelToLocation(new Microsoft.Maps.Point(_x1, _y1))
				
				se = window.maps['b1'].tryPixelToLocation(new Microsoft.Maps.Point(_x - w, _y - h))
				sw = window.maps['b1'].tryPixelToLocation(new Microsoft.Maps.Point(_x - w, _y1 - h))
				ne = window.maps['b1'].tryPixelToLocation(new Microsoft.Maps.Point(_x1 - w, _y - h))
				nw = window.maps['b1'].tryPixelToLocation(new Microsoft.Maps.Point(_x1 - w, _y1 - h))
				
				window.data = {"se": [se.latitude, se.longitude], "sw": [sw.latitude, sw.longitude], "ne": [ne.latitude, ne.longitude], "nw": [nw.latitude, nw.longitude]}
				
				break
				
			case('osm'):
				
				var pnt1 = window.maps['b1'].getLonLatFromPixel(new OpenLayers.Pixel(_x, _y)).transform(window.maps['b1'].getProjectionObject(), new OpenLayers.Projection("EPSG:4326"))
				var pnt2 = window.maps['b1'].getLonLatFromPixel(new OpenLayers.Pixel(_x1, _y1)).transform(window.maps['b1'].getProjectionObject(), new OpenLayers.Projection("EPSG:4326"))
				
				se = window.maps['b1'].getLonLatFromPixel(new OpenLayers.Pixel(_x, _y)).transform(window.maps['b1'].getProjectionObject(), new OpenLayers.Projection("EPSG:4326"))
				sw = window.maps['b1'].getLonLatFromPixel(new OpenLayers.Pixel(_x, _y1)).transform(window.maps['b1'].getProjectionObject(), new OpenLayers.Projection("EPSG:4326"))
				ne = window.maps['b1'].getLonLatFromPixel(new OpenLayers.Pixel(_x1, _y)).transform(window.maps['b1'].getProjectionObject(), new OpenLayers.Projection("EPSG:4326"))
				nw = window.maps['b1'].getLonLatFromPixel(new OpenLayers.Pixel(_x1, _y1)).transform(window.maps['b1'].getProjectionObject(), new OpenLayers.Projection("EPSG:4326"))
				
				window.data = {"se": [se.lat, se.lon], "sw": [sw.lat, sw.lon], "ne": [ne.lat, ne.lon], "nw": [nw.lat, nw.lon]}
				
				break
		}
		
		// wait for data - workaround for google maps OverlayView()
		function wait()
		{
			var method
			
			if (typeof window.data == 'object')
			{
				clearInterval(bc)
				window.data["enabled"] = true
				
				var _data = JSON.stringify(window.data)
				var self = this
				
				// update or create ?
				if (!filter_name)
				{
					var val = prompt("Filter name:")
					if (!val)
					{
						return
					}
					
					method = 'create'
				}
				else
				{
					var val = fname
					method = 'update'
				}
				
				var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
				yajax.send({
					"method": "POST",
					"url": "/filter/" + method + "/",
					"data": {"name": encodeURIComponent(val), "data": _data},
					"header": headers,
					"success": function(resp)
					{
						function get_time()
						{
							var a = geo.id('date-pick-1'), b = geo.id('date-pick-2')
							if (a && b) return [a, b]
						}
						
						window.filters_stack = JSON.parse(resp)
						//console.debug(resp)
						geo.filter.render()
						geo.render_filters()
						
						var _time = get_time()
						if (typeof _time == 'array') window.data['time'] = _time
						
						window.data['active'] = true
						
						//console.debug(window.data)
						window.data = {}
						
						var map1 = geo.get('b1')
						if (!map1 || map1 == 'undefined') map1 = 'google'
						geo.get_data()
					}
				})
			}
		}
		
		var bc = setInterval(wait, 100)
		this.edit_stop()
	}
}

/**
 * Filters filtrate
 */

geo.filters_grant_view = function(obj)
{
	//console.debug(window.filters_stack)
	if (obj)
	{
		var lat, lng, lng_start, lng_end, lat_start, lat_end
		
		var stack = []
		
		lat = obj.lat
		lng = obj.lng
		
		for (var x in filters_stack)
		{
			if (window.filters_stack[x]['geodata']['enabled'])
			{
				lat_start = window.filters_stack[x]['geodata']['nw'][0]
				lat_end = window.filters_stack[x]['geodata']['ne'][0]
				lng_start = window.filters_stack[x]['geodata']['se'][1]
				lng_end = window.filters_stack[x]['geodata']['nw'][1]
				
				if ((lat > lat_start && lat < lat_end) && (lng > lng_start && lng < lng_end))
				{
					stack.push('1')
				}
			}
		}
		
		if (stack.length > 0) return true 
	}
	return false
}

/**
 * Filter switch on / off button
 */

geo.render_filter_switch = function()
{
	var btn = document.createElement('div')
}

geo.filters_switch_init = function()
{
	if (geo.helpers.obj_size(window.filters_stack) > 0)
	{
		if (geo.get('filters') == 'on')
		{
			geo.id('filt-div').style.background = 'green'
			geo.id('filt-status').innerHTML = 'Enabled'
			geo.id('filt-checkbox').checked = 'checked'
		}
		else
		{
			geo.id('filt-div').style.background = 'brown'
			geo.id('filt-status').innerHTML = 'Disabled'
			geo.id('filt-checkbox').checked = ''
		}
	}
	else
	{
		geo.id('filt-div').style.background = 'brown'
		geo.id('filt-status').innerHTML = 'Disabled'
		geo.id('filt-checkbox').checked = ''
		geo.set('filters', 'off')
	}
}

geo.filters_switch = function()
{
	if (geo.helpers.obj_size(window.filters_stack) > 0)
	{
		if (geo.get('filters') != 'on')
		{
			geo.id('filt-div').style.background = 'green'
			geo.id('filt-status').innerHTML = 'Enabled'
			geo.id('filt-checkbox').checked = 'checked'
			geo.set('filters', 'on')
		}
		else
		{
			geo.id('filt-div').style.background = 'brown'
			geo.id('filt-status').innerHTML = 'Disabled'
			geo.id('filt-checkbox').checked = ''
			geo.set('filters', 'off')
		}
		geo.get_data()
	}
	else
	{
		alert('no filters')
		geo.id('filt-div').style.background = 'brown'
		geo.id('filt-status').innerHTML = 'Disabled'
		geo.id('filt-checkbox').checked = ''
		geo.set('filters', 'off')
	}
}

geo.render_filters = function()
{
	var map
	
	//geo.filters_switch_init()
	
	if (geo.get('filters') == 'on')
	{
		if (geo.helpers.obj_size(window.filters_stack) > 0)
		{
			for (var x in window.filters_stack)
			{
				var a = geo.get('b1')
				
				function inArray(arr, val)
				{
					for (var i = 0; i < arr.length; i++) if (val == arr[i]) return true; 
					return false;
				}
				
				switch(a)
				{
					case('google'):
						
						map = window.maps['b1']
						
						if (typeof window.maps['b1']['rect'] == 'undefined') window.maps['b1']['rect'] = {}
						
						if (window.filters_stack[x]['geodata']['enabled'] == true)
						{
							if (window.filters_stack[x]['geodata']['se'] != 'undefined')
							{
								if (!window.maps['b1']['rect'][x])
								{
									window.maps['b1']['rect'][x] = new google.maps.Rectangle({
										strokeColor: '#FF0000',
										strokeOpacity: 0.8,
										strokeWeight: 2,
										fillColor: '#FF0000',
										fillOpacity: 0.25,
										map: map,
										bounds: new google.maps.LatLngBounds(
											
											new google.maps.LatLng(parseFloat(window.filters_stack[x]['geodata']['sw'][0]), parseFloat(window.filters_stack[x]['geodata']['sw'][1])),
											
											new google.maps.LatLng(parseFloat(window.filters_stack[x]['geodata']['ne'][0]), parseFloat(window.filters_stack[x]['geodata']['ne'][1]))
											
										)
									})
								}
							}
						}
						
						break
						
					case('yandex'):
						
						if (typeof window.maps['b1']['rect'] == 'undefined') window.maps['b1']['rect'] = {}
						
						if (window.filters_stack[x]['geodata']['enabled'] == true)
						{
							if (window.filters_stack[x]['geodata']['se'] != 'undefined')
							{
								if (!window.maps['b1']['rect'][x])
								{
									window.maps['b1']['rect'][x] = new ymaps.Rectangle([
										[parseFloat(window.filters_stack[x]['geodata']['sw'][0]), parseFloat(window.filters_stack[x]['geodata']['sw'][1])],
										[parseFloat(window.filters_stack[x]['geodata']['ne'][0]), parseFloat(window.filters_stack[x]['geodata']['ne'][1])]
									], {
										fillColor: '#FF0000',
										strokeColor: '#FF0000',
										strokeWidth: 2,
										opacity: 0.25
									})
									
									window.maps['b1'].geoObjects.add(window.maps['b1']['rect'][x])
									
								}
							}
						}
						
						break
						
					case('bing'):
						
						if (typeof window.maps['b1']['rect'] == 'undefined') window.maps['b1']['rect'] = {}
						
						if (window.filters_stack[x]['geodata']['enabled'] == true)
						{
							if (window.filters_stack[x]['geodata']['se'] != 'undefined')
							{
								if (!window.maps['b1']['rect'][x])
								{
									var location1 = new Microsoft.Maps.Location(parseFloat(window.filters_stack[x]['geodata']['se'][0]), parseFloat(window.filters_stack[x]['geodata']['se'][1]))
									var location2 = new Microsoft.Maps.Location(parseFloat(window.filters_stack[x]['geodata']['ne'][0]), parseFloat(window.filters_stack[x]['geodata']['ne'][1]))
									var location3 = new Microsoft.Maps.Location(parseFloat(window.filters_stack[x]['geodata']['nw'][0]), parseFloat(window.filters_stack[x]['geodata']['nw'][1]))
									var location4 = new Microsoft.Maps.Location(parseFloat(window.filters_stack[x]['geodata']['sw'][0]), parseFloat(window.filters_stack[x]['geodata']['sw'][1]))
									
									var vertices = new Array(location1, location2, location3, location4, location1)
									
									var polygoncolor = new Microsoft.Maps.Color(100,100,0,100)
									
									window.maps['b1']['rect'][x] = new Microsoft.Maps.Polygon(vertices,{fillColor: polygoncolor, strokeColor: polygoncolor})
									
									window.maps['b1'].entities.push(window.maps['b1']['rect'][x])
								}
							}
						}
						
						break
						
					case('osm'):
						
						if (typeof window.maps['b1']['rect'] == 'undefined') window.maps['b1']['rect'] = {}
						
						if (window.filters_stack[x]['geodata']['enabled'] == true)
						{
							if (window.filters_stack[x]['geodata']['se'] != 'undefined')
							{
								if (!window.maps['b1']['rect'][x])
								{
									var style = {
										strokeColor: "#FF0000",
										strokeOpacity: 1,
										strokeWidth: 2,
										fillColor: "#FF0000",
										fillOpacity: 0.25
									}
									
									var p1 = new OpenLayers.Geometry.Point(parseFloat(window.filters_stack[x]['geodata']['se'][1]), parseFloat(window.filters_stack[x]['geodata']['se'][0])).transform(new OpenLayers.Projection("EPSG:4326"), window.maps['b1'].getProjectionObject())
									
									var p2 = new OpenLayers.Geometry.Point(parseFloat(window.filters_stack[x]['geodata']['ne'][1]), parseFloat(window.filters_stack[x]['geodata']['ne'][0])).transform(new OpenLayers.Projection("EPSG:4326"), window.maps['b1'].getProjectionObject())
									
									var p3 = new OpenLayers.Geometry.Point(parseFloat(window.filters_stack[x]['geodata']['nw'][1]), parseFloat(window.filters_stack[x]['geodata']['nw'][0])).transform(new OpenLayers.Projection("EPSG:4326"), window.maps['b1'].getProjectionObject())
									
									var p4 = new OpenLayers.Geometry.Point(parseFloat(window.filters_stack[x]['geodata']['sw'][1]), parseFloat(window.filters_stack[x]['geodata']['sw'][0])).transform(new OpenLayers.Projection("EPSG:4326"), window.maps['b1'].getProjectionObject())
									
									var pnt = []
									pnt.push(p1,p2,p3,p4)
									
									var ln = new OpenLayers.Geometry.LinearRing(pnt)
									var pf = new OpenLayers.Feature.Vector(ln, null, style)
									
									window.maps['b1']['rect'][x] = new OpenLayers.Layer.Vector(x)
									
									window.maps['b1']['rect'][x].addFeatures([pf])
									window.maps['b1'].addLayer(window.maps['b1']['rect'][x])
								}
							}
						}
						
						break
				}
			}
			
			if (!geo.get('filters')) geo.set('filters', 'off')
		}
	}
}

/**
 * Next / prev image arrows
 */

geo.arrows = 
{
	show: function()
	{
		var el, l, r, ovl
		
		el = geo.id('prime_image')
		
		ovl = document.createElement('div')
		ovl.style.width = '25px'
		ovl.style.height = el.offsetHeight + 'px'
		
		ovl1 = document.createElement('div')
		ovl1.style.width = '25px'
		ovl1.style.height = el.offsetHeight + 'px'
		
		if (!geo.id('l-arrow'))
		{
			l = document.createElement('div')
			l.id = 'l-arrow'
			l.style.position = 'absolute'
			l.style.top = 0
			l.style.left = 0
			l.style.paddingLeft = '10px'
			l.style.paddingRight = '10px'
			l.style.height = el.offsetHeight + 'px'
			l.style.display = 'none'
			l.style.background = 'url(/static/img/tr1.png)'
			l.onclick = function() { geo.image.prev() }
			ovl.style.background = 'url(/static/img/arl.png) center center no-repeat'
			ovl.style.cursor = 'pointer'
			l.appendChild(ovl)
			el.appendChild(l)
		}
		
		if (!geo.id('r-arrow'))
		{
			r = document.createElement('div')
			r.id = 'r-arrow'
			r.style.position = 'absolute'
			r.style.top = 0
			r.style.right = 0
			r.style.paddingLeft = '10px'
			r.style.paddingRight = '10px'
			r.style.height = el.offsetHeight + 'px'
			r.style.display = 'none'
			r.style.background = 'url(/static/img/tr1.png)'
			ovl1.style.background = 'url(/static/img/arr.png) center center no-repeat'
			r.onclick = function() { geo.image.next() }
			ovl1.style.cursor = 'pointer'
			r.appendChild(ovl1)
			el.appendChild(r)
		}
		
		geo.id('l-arrow').style.display = 'block'
		geo.id('r-arrow').style.display = 'block'
		
		//geo.helpers.bindEvent(geo.id('prime_image'), 'mouseout', function() { geo.arrows.hide() })
	},
	hide: function(elem)
	{
		function traverseChildren(elem)
		{
			var children = []
			var q = []
			q.push(elem)
			while (q.length > 0)
			{
				var elem = q.pop()
				children.push(elem)
				pushAll(elem.children)
			}
			
			function pushAll(elemArray)
			{
				for(var i=0;i<elemArray.length;i++)
				{
					q.push(elemArray[i])
				}
			}
			return children;
		}
		
		var list = traverseChildren(elem)
		return function onMouseOut(event) {
			var e = event.toElement || event.relatedTarget
			/*
			if (!!~list.indexOf(e))
			{
				return
			}
			*/
			
			geo.id('l-arrow').style.display = 'none'
			geo.id('r-arrow').style.display = 'none'
		}
	}
}

geo.image = {
	
	prev: function()
	{
		if (window.prev) geo.image_load(window.prev)
	},
	next: function()
	{
		if (window.next) geo.image_load(window.next)
	}
}

window.getWinSize = function()
{
	if (window.innerWidth!= undefined)
	{
		return [window.innerWidth, window.innerHeight]
	}
    else
	{
        var B = document.body, 
		D = document.documentElement
		return [Math.max(D.clientWidth, B.clientWidth), Math.max(D.clientHeight, B.clientHeight)]
	}
}

geo.resize = function()
{
	function viewport()
	{
		var e = window, a = 'inner'
		if ( !( 'innerWidth' in window ) )
		{
			a = 'client'
			e = document.documentElement || document.body
		}
		return { width : e[ a+'Width' ] , height : e[ a+'Height' ] }
	}
	
	// fit to height
	var h = ((window.getWinSize()[1] - geo.id('fit-top').offsetHeight) / 2) - 50
	
	geo.id('b1-pre').style.height = h + 'px'
	geo.id('b1').style.height = h + 'px'
	geo.id('b2-pre').style.height = h + 'px'
	geo.id('b2').style.height = h + 'px'
	
	h = ((window.getWinSize()[1] - geo.id('fit-top').offsetHeight) / 3) - 10
	geo.id('switch-content').style.height = (h - 75) + 'px'
	//geo.id('project-editor').style.height = (h - 75) + 'px'
	geo.id('filters').style.height = (h - 80) + 'px'
	geo.id('marker-settings').style.height = (h - 80) + 'px'
	
	// center footer thumbnails
	//h = (window.getWinSize()[1] - geo.id('center-footer').offsetHeight)
	//alert(geo.id('center-column').offsetHeight)
	//alert(viewport().height)
	
	h_bottom = ((viewport().height - geo.id('top-panel').offsetHeight - geo.id('photo-comments').offsetHeight - geo.id('top-level').offsetHeight) / 3) + 30
	
	//alert(h_bottom)
	h_prime = (viewport().height - geo.id('top-panel').offsetHeight - geo.id('photo-comments').offsetHeight - geo.id('top-level').offsetHeight) - h_bottom - 85
	
	geo.id('prime_image').style.height = h_prime + 'px'
	geo.id('prime_image').style.maxHeight = h_prime + 'px'
	
	//h = (viewport().height - geo.id('top-level').offsetHeight - geo.id('photo-comments').offsetHeight - geo.id('center-column').offsetHeight - 70)
	//alert(h)
	//geo.id('center-column').style.height = h + '';
	
	geo.id('thumbnails').style.height = h + 'px'
	geo.id('geodata').style.height = h + 'px'
	
	// arrows
	if (geo.id('l-arrow') && geo.id('r-arrow'))
	{
		geo.id('r-arrow').style.height = geo.id('prime-image').offsetHeight
		geo.id('l-arrow').style.height = geo.id('prime-image').offsetHeight
	}
}

/**
 * Apply settings
 */

geo.marker_settings_apply = function()
{
	if (typeof window.maps['b1'] == 'object' && typeof window.maps['b2'] == 'object')
	{
		var map1 = geo.get('b1')
		if (!map1 || map1 == 'undefined') map1 = 'google'
		var map2 = geo.get('b2')
		if (!map2 || map2 == 'undefined') map2 = 'yandex'
		
		geo.set('marker-step-size', geo.id('marker-slider-1-angle').innerHTML)
		geo.set('marker-grid-steps', geo.id('marker-slider-2-angle').innerHTML)
		
		window.markers_stack = geo.calc_additional_params(window.markers_stack)
		
		geo.change_map('b1', map1)
		geo.change_map('b2', map2)
	}
}

geo.marker_settings = function()
{
	var el = geo.id('marker-settings')
	function implement()
	{
		var a, b, c, d, el, mark, mark1
		
		el = geo.id('marker-settings')
		
		// step size
		//el.innerHTML = '<table onclick="geo.block.switch_multiple()" style="cursor: pointer;margin: 7px 0"><tr><td><input type="checkbox"></td><td>Show all markers</td></tr></table>'
		el.innerHTML = '<div>Step size:</div>'
		a = document.createElement('div')
		a.id = 'marker-slider-1'
		a.setAttribute('class', 'dragdealer')
		b = document.createElement('div')
		b.id = 'marker-slider-1-angle'
		b.setAttribute('class', 'red-bar handle')
		el.appendChild(a)
		a.appendChild(b)
		
		// number of grid steps
		el.innerHTML += '<div>Number of grid steps:</div>'
		c = document.createElement('div')
		c.id = 'marker-slider-2'
		c.setAttribute('class', 'dragdealer')
		d = document.createElement('div')
		d.id = 'marker-slider-2-angle'
		d.setAttribute('class', 'red-bar handle')
		el.appendChild(c)
		c.appendChild(d)
		
		el.innerHTML += '<div style="text-align: right;margin-top: 5px"><div class="button black" style="width: 80px;display: inline-block" onclick="geo.marker_settings_apply()">Apply</div></div>'
		
		moffset = geo.get('marker-step-size')
		mnumarcs = geo.get('marker-grid-steps')
		
		if (!moffset)
		{
			moffset = 20
			geo.set('marker-step-size', 20)
		}
		
		if (!mnumarcs)
		{
			mnumarcs = 5
			geo.set('marker-grid-steps', 5)
		}
		
		var mark = new Dragdealer('marker-slider-1', {
			steps: 10,
			snap: true,
			loose: true,
			animationCallback: function(x, y)
			{
				var angle = Math.round(x * 10) * 10
				//geo.set('marker-step-size', angle / 10)
				geo.id('marker-slider-1-angle').innerHTML = angle
			}
		})
		
		mark.setStep(moffset / 10)
		
		var mark1 = new Dragdealer('marker-slider-2', {
			steps: 10,
			snap: true,
			loose: true,
			animationCallback: function(x, y)
			{
				var angle = Math.round(x * (10 - 1)) + 1
				//geo.set('marker-grid-steps', angle)
				geo.id('marker-slider-2-angle').innerHTML = angle
			}
		})
		
		mark1.setStep(mnumarcs)
	}
	
	if (typeof Dragdealer != 'function')
	{
		geo.load_script("http://"+window.location.hostname+'/static/js/dragdealer.js?nocache=' + geo.helpers.timestamp(), function(i){
			
			if (typeof Dragdealer == 'function')
			{
				clearInterval(i)
				implement()
			}
		})
	}
	else
	{
		implement()
	}
}

geo.block = {
	
	switch_geodata: function(a)
	{
		geo.id('thumb-panel').style.background = 'url(/static/img/bg-panel-header.gif) left bottom repeat-x #464646'
		geo.id('thumbnails').style.display = 'none'
		geo.id('geodata-panel').style.background = '#555'
		geo.id('geodata').style.display = 'block'
	},
	switch_thumbnails: function(a)
	{
		geo.id('geodata-panel').style.background = 'url(/static/img/bg-panel-header.gif) left bottom repeat-x #464646'
		geo.id('thumbnails').style.display = 'block'
		geo.id('geodata').style.display = 'none'
		geo.id('thumb-panel').style.background = '#555'
	}
}

geo.project_edit = {
	
	start: function()
	{
		var id, desc, name, div
		
		div = geo.id('switch-content')
		
		for (var item in div.childNodes)
		{
			if (typeof div.childNodes[item] == 'object')
			{
				if (div.childNodes[item].getAttribute('class') == 'file active')
				{
					id = div.childNodes[item].getAttribute('itemId')
					name = div.childNodes[item].innerHTML
					desc = decodeURIComponent(div.childNodes[item].getAttribute('desc'))
				}
			}
		}
		
		//if (typeof window.env['working_project'] != 'undefined')
		//{
		geo.id('project-editor').style.display = 'block'
		//geo.id('switch-content').style.display = 'none'
		//geo.id('project_ops').style.display = 'none'
		geo.id('group-1').style.display = 'none'
		//console.debug(window.env)
		
		//if (geo.id('file_ops').style.display == 'block') geo.id('file_ops').style.display = 'none'
		
		geo.id('project-edit-id').value = id
		geo.id('project-edit-name').value = name
		geo.id('project-edit-name-old').innerHTML = name
		geo.id('project-edit-desc').innerHTML = decodeURIComponent(desc)
		//}
	},
	stop: function()
	{
		geo.id('project-editor').style.display = 'none'
		//geo.id('switch-content').style.display = 'block'
		//geo.id('project_ops').style.display = 'block'
		geo.id('group-1').style.display = 'block'
	},
	save: function()
	{
		var prj, desc, id
		
		id = geo.id('project-edit-id').value
		old_name = geo.id('project-edit-name-old').innerHTML
		prj = geo.id('project-edit-name').value
		desc = geo.id('project-edit-desc').value
		self = this
		var headers = {"X-CSRFToken": geo.helpers.readCookie('csrftoken')}
		yajax.send({
			"method": "POST",
			"url": "/project/save/",
			"data": {"name": encodeURIComponent(prj), "id": id, "desc": encodeURIComponent(desc)},
			"header": headers,
			"success": function(response)
			{
				if (response != '0')
				{
					//alert(old_name + '/' + geo.get('working_project'))
					if (old_name == geo.get('working_project'))
					{
						geo.id('project-name').innerHTML = prj
						geo.set('working_project', prj)
					}
					window.projects = JSON.parse(response)
					geo.render_projects('switch-content', window.projects)
					self.stop()
				}
				else
				{
					alert('error: 55')
				}
			}
		})
	},
	_switch: function()
	{
		if (geo.id('project-editor').style.display == 'block') this.stop(); else this.start();
	}
}

geo.init = function()
{
	window.globals = {}
	
	// csrf cookie set before launch
	a = this.helpers.readCookie()
	if (a == null || a == '') this.helpers.setCookie(this.opts.csrf_cookie, window.csrf_token)
	
	// logout button
	var btn_logout = geo.id('btn-logout')
	this.helpers.bindEvent(btn_logout, 'click', function() {window.location.href="/account/logout"})
	
	// upload button
	//var btn_upload = geo.id('btn_upload')
	//this.helpers.bindEvent(btn_upload, 'click', function() { geo.id('file_upload').click() })
	
	// upload hidden input field
	var file_upload = geo.id('file_upload')
	
	this.helpers.bindEvent(file_upload, 'change', this.handleFileUpload)
	
	// project edit
	this.helpers.bindEvent(geo.id('new_project'), 'click', geo.project_create)
	
	// project delete
	this.helpers.bindEvent(geo.id('project_delete'), 'click', geo._project_delete)
	
	// file delete
	this.helpers.bindEvent(geo.id('file_delete'), 'click', geo._image_delete)
	
	// file download
	this.helpers.bindEvent(geo.id('file_download'), 'click', geo.file_download)
	
	this.helpers.bindEvent(geo.id('filter-create'), 'click', function() { geo.filter.create() })
	this.helpers.bindEvent(geo.id('filter-delete'), 'click', function() { geo.filter.del() })
	this.helpers.bindEvent(geo.id('filter-edit'), 'click', function() { geo.filter.edit() })
	
	// next, previous prime image arrows
	this.helpers.bindEvent(geo.id('prime_image'), 'mouseover', function() { geo.arrows.show() })
	this.helpers.bindEvent(geo.id('prime_image'), 'mouseout', geo.arrows.hide(geo.id('prime_image')))
	
	// map1 fullscreen mode
	//this.helpers.bindEvent(geo.id('map1_fullscreen'), 'click', function() {alert('fullscreen map1')})
	// map2 fullscreen mode
	//this.helpers.bindEvent(geo.id('map2_fullscreen'), 'click', function() {alert('fullscreen map2')})
	
	/*
	if (typeof window.opera == 'object')
	{
		geo.id('opera_submit').style.display = 'block'
	}
	*/
	
	var data = this.get_data()
	geo.resize()
}

