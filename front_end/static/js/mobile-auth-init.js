// initialize jo
jo.load();

var stack = new joStack();
var scn = new joScreen(
	
			new joContainer([
				new joFlexcol([
					this.nav = new joNavbar(),
					this.stack = new joStackScroller()
				]),
				this.toolbar = new joToolbar("")
			]).setStyle({position: "absolute", top: "0", left: "0", bottom: "0", right: "0"})
		
	);

// create our view card, notice we're nesting widgets inline
var card = new joCard([
    new joTitle("geo-photo.net"),
    new joCaption("Connect via:"),
    new joDivider(),
    new joButton("Facebook").selectEvent.subscribe(function() {
		window.open("http://deploy.geo-photo.net/oauth/facebook","facebook_window","menubar=1,resizable=1")
    }),
    new joButton("Google").selectEvent.subscribe(function() {
		window.open("http://deploy.geo-photo.net/oauth/google","google_window","menubar=1,resizable=1")
	}),
    new joButton("Twitter").selectEvent.subscribe(function() {
		window.open("http://deploy.geo-photo.net/oauth/twitter","twit_window","menubar=1,resizable=1")
    }),
    new joButton("geo-photo.net").selectEvent.subscribe(function() {
		window.open("http://deploy.geo-photo.net/oauth/self","twit_window","menubar=1,resizable=1")
	})
]);

// setup our stack and screen

// put the card on our view stack
stack.push(card);
