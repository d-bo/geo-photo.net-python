# -*- coding: utf-8 -*-

from django.db import models


""" Projects """
class projects(models.Model):
    project_id = models.AutoField(primary_key=True)
    project_name = models.CharField(max_length=200, blank=True, null=True)
    project_user_id = models.IntegerField(blank=True, null=True)
    project_description = models.TextField(max_length=2000, blank=True, null=True)
    project_hash = models.CharField(max_length=1000, blank=True, null=True)
    project_create_date = models.IntegerField(blank=True, null=True)
    project_is_deleted = models.NullBooleanField(blank=True, null=True)
    project_short_url = models.CharField(max_length=200, blank=True, null=True)


""" All the images """
class image(models.Model):
    image_id = models.AutoField(primary_key=True)
    image_filename = models.CharField(max_length=5000, blank=True, null=True)
    image_project_id = models.IntegerField(blank=True, null=True)
    image_user_id = models.IntegerField(blank=True, null=True)
    image_lat = models.FloatField(blank=True, null=True)
    image_lng = models.FloatField(blank=True, null=True)
    image_user_lat = models.FloatField(blank=True, null=True)
    image_user_lng = models.FloatField(blank=True, null=True)
    image_gps_lat = models.FloatField(blank=True, null=True)
    image_gps_lng = models.FloatField(blank=True, null=True)
    image_upload_date = models.IntegerField(blank=True, null=True)
    image_create_date = models.CharField(max_length=1000,blank=True, null=True)
    image_file_size = models.IntegerField(blank=True, null=True)
    image_json = models.TextField(max_length=10000, blank=True, null=True)
    image_is_deleted = models.NullBooleanField(blank=True, null=True)
