# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import Context
from django.template import RequestContext
from django.shortcuts import redirect
from django import forms
from django.conf import settings
import django.middleware.csrf

from account.views import load_session
from account.views import save_session

from projects.models import shared
from account.models import sessions
from front_end.models import projects

import subprocess
import urllib
import json
import datetime
import hashlib
import os
import os.path
from urllib import unquote


class UploadFileForm(forms.Form):
    files = forms.FileField()



def route_id(request, user_id):
    
    """ Route to user url """
    
    now = datetime.datetime.now()
    if request.user.is_authenticated():
        if str(user_id) == str(request.user.id):
            return render_to_response('dashboard2.html', {'user_name': request.user.username.encode('utf8'), 'user_id': request.user.id}, context_instance=RequestContext(request))
        else:
            return redirect('/id' + str(request.user.id) + '/')
    else:
        return redirect('/')


def index(request):
    
    if request.user.is_authenticated():
        return redirect('/id' + str(request.user.id) + '/')
    else:
        now = datetime.datetime.now()
        return render_to_response('login_dev.html', {'current_date': now}, context_instance=RequestContext(request))


def login_page(request):
    
    """ Render login page """
    
    return render_to_response('login_dev.html', {'current_date': now}, context_instance=RequestContext(request))


def dashboard(request):
    
    """ Render dashboard """
    
    if request.user.is_authenticated():
        load_session(request)
        #c = django.middleware.csrf.get_token()
        return render_to_response('dashboard1.html', {'user_name': request.user.username.encode('utf8'), 'user_id': request.user.id}, context_instance=RequestContext(request))
    else:
        return redirect('front_end.views.index')


def dashboard1(request):
    
    """ Render dashboard testing """
    
    if request.user.is_authenticated():
        #c = django.middleware.csrf.get_token()
        return render_to_response('dashboard2.html', {'user_name': request.user.username.encode('utf8'), 'user_id': request.user.id}, context_instance=RequestContext(request))
    else:
        return redirect('front_end.views.index')


def storage_jpeg(request, picnumber):
    
    """ Get file from storage """
    
    user_root = get_path(request)
    return HttpResponse(picnumber)


def get_path(request, user_id=False):
    
    """ get path to project work directory (without '/' at the end) """
    
    if settings.PROJECTS_ROOT_DIR:
        if request.user.is_authenticated():
            
            """ Check if the user folder exists. None - attempt to create it """
            user_dir = hashlib.md5(request.user.username.encode('utf8')).hexdigest()
            if os.path.isdir(settings.PROJECTS_ROOT_DIR + user_dir):
                # user dir exist
                #return pprint.pprint(request.GET)
                path = settings.PROJECTS_ROOT_DIR + user_dir
            else:
                # user dir not exist - attempt to create it
                os.makedirs(settings.PROJECTS_ROOT_DIR + user_dir)
                path = settings.PROJECTS_ROOT_DIR + user_dir
            
            return path
            
        else:
            #return HttpResponse('no auth')
            if user_id != False:
                #get user by id
                name = get_user_by_id(user_id)
                if name != False:
                    user_dir = hashlib.md5(name.encode('utf8')).hexdigest()
                    if os.path.isdir(settings.PROJECTS_ROOT_DIR + user_dir):
                        # user dir exist
                        #return pprint.pprint(request.GET)
                        path = settings.PROJECTS_ROOT_DIR + user_dir
                    else:
                        # user dir not exist - attempt to create it
                        os.makedirs(settings.PROJECTS_ROOT_DIR + user_dir)
                        path = settings.PROJECTS_ROOT_DIR + user_dir
                        
                    return path
                else:
                    return False
            else:
                return False


def get_user_by_id(user_id):
    
    """ get user by id """
    
    from django.contrib.auth.models import User
    try:
        a = User.objects.get(id=user_id)
    except:
        return False
    else:
        return a.username


def upload(request):
    
    """ Processing files upload """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            
            import time
            timestamp = time.time()
            
            try:
                load_session(request)
                request.session['data']['working_project']
            except:
                
                # check if there any projects created
                try:
                    prj = projects.objects.filter(project_user_id=request.user.id)
                except:
                    return HttpResponse('No projects created')
                else:
                    if (len(prj) > 0):
                        return HttpResponse('there are some projects')
                    else:
                        return HttpResponse(4)
                    
                """
                if len(prj) > 0:
                    return HttpResponse(request.session['data']['working_project'])
                else:
                    return HttpResponse("you need to create at least one project")
                """
                
            else:
                # is that project in the table
                prj = projects.objects.get(project_id=request.session['data']['working_project'])
                filename = request.META['HTTP_X_FILENAME'].encode('utf8')
                if prj:
                    
                    """ check image size """
                    size = request.FILES['file'].size
                    #return HttpResponse('file too heavy')
                    
                    """ check if image is already loaded """
                    from front_end.models import image
                    img = image.objects.filter(image_user_id=request.user.id, image_filename=urllib.unquote(filename), image_project_id=request.session['data']['working_project'], image_is_deleted=None)
                    if len(img) > 0:
                        return HttpResponse(2)
                    
                    """ Saving to file system """
                    if get_path(request) != 0:
                        
                        # storage filename saves as a record id from image table
                        
                        path = get_path(request) + '/' + prj.project_hash + '/' + filename
                        
                        with open(path, 'wb+') as destination:
                            for chunk in request.FILES['file'].chunks():
                                destination.write(chunk)
                    else:
                        return HttpResponse('path not detected')
                        
                    if os.path.isfile(path):
                        
                        supported_imgs = ['jpg', 'JPG', 'jpeg', 'JPEG']
                        
                        import imghdr
                        
                        # determine image header
                        file_type = imghdr.what(path)
                        if file_type != None:
                            
                            #exec_path = "/home/coder/libsgeo/test -r "+ urllib2.unquote(path.replace(' ', '\ '))
                            exec_path = settings.LIBSGEO_EXEC_PATH + "test -r "+ path
                            #return HttpResponse(exec_path)
                            geodata = subprocess.check_output(exec_path,stderr=subprocess.STDOUT,shell=True)
                            if len(geodata) > 1:
                                parse_json = json.loads(geodata)
                                
                                from PIL.ExifTags import TAGS
                                
                                try:
                                    img = Image.open(path)
                                    if hasattr(img, '_getexif'):
                                        exifinfo = img._getexif()
                                        if exifinfo != None:
                                            for tag, value in exifinfo.items():
                                                decoded = TAGS.get(tag, tag)
                                                ret[decoded] = value
                                except:
                                    exif = {}
                                else:
                                    exif = ret
                                #return ret
                                
                                if parse_json:
                                    
                                    # save geodata and a filename in a table and give it unique id
                                    # to pull it later
                                    parse_json['exifer'] = {}
                                    parse_json['exifer'] = exif
                                    #timestamp = datetime.now()
                                    
                                    img = image(
                                        image_filename=urllib.unquote(filename),
                                        image_file_size=request.FILES['file'].size,
                                        image_user_id=request.user.id,
                                        image_lat=parse_json['Latitude'],
                                        image_lng=parse_json['Longitude'],
                                        image_user_lat=parse_json['UserLatitude'],
                                        image_user_lng=parse_json['UserLongitude'],
                                        image_gps_lat=parse_json['GPSLatitude'],
                                        image_gps_lng=parse_json['GPSLongitude'],
                                        image_create_date=parse_json['DateTimeOriginal'],
                                        image_project_id=request.session['data']['working_project'],
                                        image_upload_date=timestamp,
                                        image_json = geodata
                                    )
                                    img.save()
                                    return HttpResponse(parse_json['DateTimeOriginal'])
                            else:
                                # there is no geodata in the file
                                img = image(
                                    image_filename=urllib.unquote(filename),
                                    image_file_size=request.FILES['file'].size,
                                    image_project_id=request.session['data']['working_project'],
                                    image_user_id=request.user.id,
                                    image_upload_date=timestamp
                                )
                                img.save()
                        else:
                            return HttpResponse('not an image file')
                            
                        return HttpResponse(0)
                    else:
                        return HttpResponse(1)
                else:
                    return HttpResponse('no projects found')
            #else:
                #return HttpResponse('invalid data')


def project_create(request):
    
    """ Create project """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            
            try:
                prj = projects.objects.get(project_user_id=request.user.id, project_name=request.POST['name'].encode('utf_8'),project_is_deleted=None)
            except:
                import time
                timestamp = round(time.time())
                prj_hash  = hashlib.md5(request.POST['name'].encode('utf_8') + str(timestamp)).hexdigest()
                
                # add project to table
                prj = projects(
                    project_user_id=request.user.id,
                    project_name=request.POST['name'].encode('utf_8'),
                    project_description=request.POST['desc'].encode('utf_8'),
                    project_create_date=timestamp,
                    project_hash=prj_hash
                )
                prj.save()
                
                # create project folder
                user_dir = hashlib.md5(request.user.username.encode('utf8')).hexdigest()
                #if os.path.isdir(settings.PROJECTS_ROOT_DIR + user_dir):
                os.makedirs(settings.PROJECTS_ROOT_DIR + user_dir + '/' + prj_hash)
                if os.path.isdir(settings.PROJECTS_ROOT_DIR + user_dir + '/' + prj_hash):
                    request.session['data'] = {}
                    request.session['data']['working_project'] = prj.project_id
                    save_session(request)
                # switch this project to current status
                return HttpResponse('created ' + str(request.session['data']['working_project']))
            else:
                return HttpResponse('1')


def project_save(request):
    
    """ Save project (updated) """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            
            name = unquote(request.POST['name']).encode('utf-8')
            prj_id = unquote(request.POST['id'])
            desc = unquote(request.POST['desc']).encode('utf-8')
            
            try:
                a = projects.objects.get(project_user_id=request.user.id, project_id = prj_id)
            except:
                return HttpResponse(1)
            else:
                a.project_name = name
                a.project_description = desc
                a.save()
                return HttpResponse(json.dumps(get_projects(request)))


def project_delete(request):
    
    """ Delete project """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            
            import urllib
            
            name = urllib.unquote(request.POST['name'].encode('utf_8'))
            if (len(name) > 0):
                try:
                    prj = projects.objects.filter(project_user_id=request.user.id, project_name=name, project_is_deleted=None).update(project_is_deleted=True)
                except:
                    return HttpResponse('no projects found')
                else:
                    pass
                
                # clear session
                try:
                    a = sessions.objects.get(session_user_id=request.user.id)
                except:
                    return HttpResponse('not found')
                else:
                    b = json.loads(a.session_data)
                    if b.has_key('working_project'):
                        del b['working_project']
                    if b.has_key('working_image'):
                        del b['working_image']
                    a.session_data = json.dumps(b)
                    a.save()
                
                # load first project
                prjs = projects.objects.all()[:1].get()
                
                return HttpResponse(json.dumps(get_projects(request)))


def image_delete(request):
    
    """ Delete image from project """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            from front_end.models import image
            import urllib
            
            #name = urllib2.unquote(request.POST['name'].encode('utf_8'))
            item_id = request.POST['id']
            if (len(item_id) > 0):
                try:
                    img = image.objects.get(image_user_id=request.user.id, image_id=request.POST['id'], image_is_deleted=None)
                except:
                    return HttpResponse('requested image not found')
                else:
                    img.image_is_deleted = True
                    img.save()
                    
                    # load first project
                    prjs = projects.objects.all()[:1].get()
                    
                    return HttpResponse(0)


def api_env(request):
    
    """ Get environment """
    
    if request.method == 'GET':
        #if request.user.is_authenticated():
            from filters.views import get_filters
            
            if request.GET.has_key('shared_project'):
                try:
                    p = int(request.GET['shared_project'])
                    user = int(request.GET['shared_user_id'])
                except:
                    p = False
                    user = False
                else:
                    pass
            
            load_session(request)
            if request.GET['get'] == 'files':
                out = get_files(request, p, user)
            elif request.GET['get'] == 'filters':
                out = get_filters(request, p, user)
            elif request.GET['get'] == 'markers':
                out = get_markers(request, p, user)
            elif request.GET['get'] == 'projects':
                out = get_projects(request)
            elif request.GET['get'] == 'all':
                out = {}
                out['files']    = get_files(request, p, user)
                out['projects'] = get_projects(request)
                out['markers']  = get_markers(request, p, user)
                out['filters']  = get_filters(request, p, user)
                out['env'] = {}
                
                try:
                    prj_name = projects.objects.get(project_user_id=request.user.id, project_id=request.session['data']['working_project'], project_is_deleted=None)
                except:
                    pass
                else:
                    out['env']['working_project'] = prj_name.project_name
                    out['env']['working_project_description'] = prj_name.project_description
                    
                    b = is_project_shared(prj_name.project_name, request.user.id)
                    
                    if b != False:
                        out['env']['is_project_shared'] = True
                    else:
                        out['env']['is_project_shared'] = False
                    
            else:
                out = {}
            
            return HttpResponse(json.dumps(out))


def get_files(request, shared=False, shared_user_id=False):
    
    """ Get files data in JSON """
    
    from front_end.models import image
    try:
        if shared !=False:
            prj = int(shared)
            user_id = int(shared_user_id)
            if is_project_shared_id(prj, user_id) != False:
                pass
            else:
                return HttpResponse('not a shared project')
        else:
            prj = request.session['data']['working_project']
            user_id = request.user.id
    except:
        return False
    else:
        # TODO: filter checking embed
        img = image.objects.filter(image_project_id=prj,image_user_id=user_id, image_is_deleted=None).order_by('image_filename')
        #return HttpResponse('wwwww')
        if type(img).__name__ == 'QuerySet':
            out = {}
            z = 0
            for x in img:
                out[z] = {}
                out[z]['name'] = x.image_filename
                out[z]['id'] = x.image_id
                z = z+1
            return out
        else:
            a = type(img).__name__
            return {"not": a}


def get_projects(request):
    
    """ Get projects data in JSON """
    
    import urllib
    
    try:
        q = projects.objects.filter(project_user_id=request.user.id,project_is_deleted=None)
    except:
        return False
    else:
        if type(q).__name__ == 'QuerySet':
            out = {}
            for x in q:
                name = urllib.quote(x.project_name.encode('utf-8'))
                out[name] = {}
                out[name]['id'] = x.project_id
                out[name]['desc'] = x.project_description
            return out
        else:
            a = type(q).__name__
            return {"not": a}


def project_share(request):
    
    """ Switch project shared status """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            name = unquote(request.POST['name'].encode('utf-8'))
            
            try:
                _prj_id = projects.objects.get(project_name=name, project_user_id=request.user.id)
            except:
                return HttpResponse('not exist')
            else:
                prj_id = _prj_id.project_id
            
            try:
                a = shared.objects.get(shared_user_id=request.user.id,shared_project_name=request.POST['name'])
            except:
                b = shared(shared_project_name=request.POST['name'], shared_user_id=request.user.id, shared_project_id=prj_id)
                b.save()
            else:
                a.delete()
            
            return HttpResponse(0)


def project_load(request):
    
    """ Load project (ajax) """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            
            import urllib
            from filters.views import get_filters
            
            try:
                prj = projects.objects.get(project_user_id=request.user.id, project_id=urllib.unquote(request.POST['id']), project_is_deleted=None)
            except:
                return HttpResponse(0)
            else:
                out = {}
                try:
                    request.session['data']['working_project'] = prj.project_id
                except:
                    request.session['data'] = {}
                    request.session['data']['working_project'] = prj.project_id
                else:
                    pass
                save_session(request)
                out['env'] = {}
                out['env']['working_project'] = prj.project_name
                out['env']['working_project_description'] = prj.project_description
                
                # shared project or not
                if is_project_shared(prj.project_name, request.user.id) != False:
                    out['env']['is_project_shared'] = True
                else:
                    out['env']['is_project_shared'] = False
                
                out['files'] = get_files(request)
                out['filters'] = get_filters(request)
                out['markers'] = get_markers(request)
                out['projects'] = get_projects(request)
                
                return HttpResponse(json.dumps(out))


def is_project_shared(prj_name, user_id):
    
    """ Check if the project is shared to others """
    
    try:
        a = shared.objects.get(shared_user_id=user_id, shared_project_name=prj_name)
    except:
        return False
    else:
        return True


def is_project_shared_id(prj_id, user_id):
    
    """ Check if the project is shared to others by ID """
    
    try:
        a = shared.objects.get(shared_user_id=user_id, shared_project_id=prj_id)
    except:
        return False
    else:
        return True
        

def get_markers(request, shared=False, shared_user_id=False):
    
    """ Get project geo markers """
    
    #if request.user.is_authenticated():
        try:
            if shared !=False:
                prj = int(shared)
                user_id = int(shared_user_id)
                if is_project_shared_id(prj, user_id) != False:
                    pass
                else:
                    return HttpResponse('not a shared project')
            else:
                prj = request.session['data']['working_project']
                user_id = request.user.id
        except:
            out = {}
        else:
            usr_id=user_id
            prj_id=prj
            from front_end.models import image
            out = {}
            try:
                q=image.objects.filter(image_user_id=usr_id, image_project_id=prj_id, image_is_deleted=None)
            except:
                pass
            else:
                import urllib
                if type(q).__name__ == 'QuerySet':
                    for b in q:
                        i = urllib.quote(b.image_filename)
                        out[i] = {}
                        out[i]['lat'] = b.image_lat
                        out[i]['lng'] = b.image_lng
                        out[i]['user_lat'] = b.image_user_lat
                        out[i]['user_lng'] = b.image_user_lng
                        out[i]['gps_lat'] = b.image_gps_lng
                        try:
                            img_json = json.loads(b.image_json)
                        except:
                            pass
                        else:
                            out[i]['az'] = img_json['Azimuth']
                            out[i]['roll'] = img_json['Roll']
                            out[i]['pitch'] = img_json['Pitch']
                            out[i]['h_angle'] = img_json['HViewAngle']
                            out[i]['v_angle'] = img_json['VViewAngle']
                else:
                    out = {}
        return out


def image_get(request, name):
    
    """ Central image processing """
    
    #if request.user.is_authenticated():
        if request.method == 'GET':
            load_session(request)
            import urllib
            _file_id = urllib.unquote(name)
            file_id = _file_id.replace(".jpg", "")
            max_w_h = 1000
            
            if request.GET.has_key('prj_id') and request.GET.has_key('user_id'):
                prj = int(request.GET['prj_id'])
                user_id = int(request.GET['user_id'])
                
                if is_project_shared_id(prj, user_id) != False:
                    pass
                else:
                    return HttpResponse('not a shared project')
                
            else:
                if request.user.is_authenticated():
                    prj = request.session['data']['working_project']
                    user_id = request.user.id
                else:
                    return HttpResponse('no auth')
            
            try:
                prj = projects.objects.get(project_id=prj, project_user_id=user_id)
            except:
                return HttpResponse(1)
            else:
                try:
                    from front_end.models import image
                    img = image.objects.get(image_id=file_id)
                except:
                    return HttpResponse(2)
                else:
                    _filename = urllib.quote(img.image_filename)
                    filename = _filename.replace(' ', '%20')
                    
                    import Image
                    # is there a cached-resized image ?
                    path = get_path(request, user_id) + '/' + prj.project_hash + '/_' + str(max_w_h) + '_' + filename
                    if os.path.isfile(path):
                        
                        request.session['data']['working_image'] = filename
                        im = Image.open(path)
                        
                        out = HttpResponse(mimetype="image/jpeg")
                        im.save(out, "JPEG")
                        return out
                        #return HttpResponse(path)
                    else:
                        # maybe there is no need to resize image cause it not so big
                        path = get_path(request, user_id) + '/' + prj.project_hash + '/' + filename
                        #return HttpResponse(path)
                        if os.path.isfile(path):
                            im = Image.open(path)
                            w, h = im.size
                            if w > max_w_h or h > max_w_h:
                                im = image_resize(im, max_w_h)
                                path = get_path(request, user_id) + '/' + prj.project_hash + '/_' + str(max_w_h) + '_' + filename
                                im.save(path)
                                
                                out = HttpResponse(mimetype="image/jpeg")
                                im.save(out, "JPEG")
                                return out
                            else:
                                request.session['data']['working_image'] = filename
                                im = Image.open(path)
                                
                                out = HttpResponse(mimetype="image/jpeg")
                                im.save(out, "JPEG")
                                return out
                        else:
                            return HttpResponse('error: m1')
                #path = get_path(request) + '/' + prj.project_hash + '/' + filename
                return HttpResponse(3)
        else:
            return HttpResponse('not a get request')
    #else:
        #return HttpResponse('not auth')


def image_resize(obj, max_size):
    
    """ Resize image constrain proportions """
    
    import PIL
    from PIL import Image
    basewidth = max_size
    img = obj
    wpercent = (basewidth/float(img.size[0]))
    hsize = int((float(img.size[1])*float(wpercent)))
    img = img.resize((basewidth,hsize), PIL.Image.ANTIALIAS)
    return img


def image_data(request):
    
    """ Get image data """
    
    #if request.user.is_authenticated():
        if request.method == 'GET':
            load_session(request)
            
            if request.GET.has_key('user_id'):
                user_id = request.GET['user_id']
            else:
                user_id = request.user.id
            
            
            from front_end.models import image
            out = {}
            try:
                data = image.objects.get(image_id=request.GET['id'], image_user_id=user_id, image_is_deleted=None)
            except:
                return HttpResponse(1)
            else:
                if data.image_json != None:
                    json_arr = json.loads(data.image_json)
                    json_arr['name'] = data.image_filename
                    return HttpResponse(json.dumps(json_arr))
                else:
                    return HttpResponse(1)


def image_thumbnails(request):
    
    """ Get thumbnail """
    
    #if request.user.is_authenticated():
        if request.method == 'GET':
            load_session(request)
            import urllib
            file_id = urllib.unquote(request.GET['id'].encode('utf_8'))
            
            try:
                if request.GET.has_key('prj_id') and request.GET.has_key('user_id'):
                    prj = int(request.GET['prj_id'])
                    user_id = int(request.GET['user_id'])
                    if is_project_shared_id(prj, user_id) != False:
                        pass
                    else:
                        return HttpResponse('not a shared project')
                else:
                    prj = request.session['data']['working_project']
                    user_id = request.user.id
                
                prj = projects.objects.get(project_id=prj, project_user_id=user_id)
                
            except:
                return HttpResponse(1)
            else:
                try:
                    from front_end.models import image
                    img = image.objects.get(image_id=request.GET['id'].encode('utf_8'))
                except:
                    return HttpResponse(2)
                else:
                    _filename = img.image_filename
                    filename = _filename.replace(' ', '%20')
                    path = get_path(request, user_id) + '/' + prj.project_hash + '/' + filename
                    
                    if os.path.isfile(path):
                        import Image
                        
                        # check if the image is already cached
                        thumb_path = get_path(request, user_id) + '/' + prj.project_hash + '/thumbnails/' + file_id + '.jpg'
                        if os.path.isfile(thumb_path):
                            im = Image.open(thumb_path)
                            out = HttpResponse(mimetype="image/jpeg")
                            im.save(out, "JPEG")
                            return out
                        else:
                            im = Image.open(path)
                            out = HttpResponse(mimetype="image/jpeg")
                            #_im = im.resize((80,80), Image.ANTIALIAS)
                            th_dir = get_path(request, user_id) + '/' + prj.project_hash + '/thumbnails/'
                            if os.path.isdir(th_dir) == False:
                                os.mkdir(th_dir)
                            _im = create_thumbnail(80, 80, im)
                            _im.save(thumb_path)
                            _im.save(out, "JPEG")
                            
                            return out
                    else:
                        return HttpResponse('file not exist')
                return HttpResponse(3)
        else:
            return HttpResponse('not a get request')
    #else:
        #return HttpResponse('not auth')


def create_thumbnail(x, y, im):
    
    """ Calculate thumbnails / return object """
    
    import Image
    
    THUMB_SIZE = x, y
    img = im
    width, height = img.size
    
    if width > height:
       delta = width - height
       left = int(delta/2)
       upper = 0
       right = height + left
       lower = height
    else:
       delta = height - width
       left = 0
       upper = int(delta/2)
       right = width
       lower = width + upper
    
    """
    w = im.size[0]
    h = im.size[1]
    
    if w > h:
        percent = (h - y) / 100
        orig_w = w / percent
        orig_h = y
        c = (orig_w - x) / 2
        d = 0
    else:
        percent = (w - x) / 100
        orig_w = y
        orig_h = h / percent
        d = (h / 2) - (orig_h / 2)
        c = 0
    
    box = (c, d, x + c, y + d)
    size = orig_w, orig_h
    im = im.resize(size)
    im = im.crop(box)
    """
    
    img = img.crop((left, upper, right, lower))
    img.thumbnail(THUMB_SIZE, Image.ANTIALIAS)
    return img


def image_write_data(request):
    
    """ Write geo data to image """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            load_session(request)
            
            try:
                prj = projects.objects.get(project_id=request.session['data']['working_project'], project_user_id=request.user.id)
            except:
                return HttpResponse(1)
            else:
                try:
                    from front_end.models import image
                    img = image.objects.get(image_filename=request.POST['name'], image_project_id = request.session['data']['working_project'], image_user_id = request.user.id)
                except:
                    return HttpResponse(2)
                else:
                    import urllib
                    filename = urllib.quote(img.image_filename)
                    path = get_path(request) + '/' + prj.project_hash + '/' + filename
                    if os.path.isfile(path):
                        # form the execute path
                        add = '--file ' + path
                        
                        if request.POST.has_key('lat'):
                            add += ' --lat ' + request.POST['lat']
                            img.image_lat = request.POST['lat']
                            
                        if request.POST.has_key('lng'):
                            add += ' --lng ' + request.POST['lng']
                            img.image_lng = request.POST['lng']
                            
                        if request.POST.has_key('az'):
                            add += ' --az ' + request.POST['az']
                            try:
                                img_json = json.loads(img.image_json)
                            except:
                                pass
                            else:
                                img_json['Azimuth'] = request.POST['az']
                                img.image_json = json.dumps(img_json)
                                
                        if request.POST.has_key('comments'):
                            add += ' --comments "' + request.POST['comments'] + '"'
                            
                        img.save()
                        exec_path = settings.LIBSGEO_EXEC_PATH + "write " + add
                        #return HttpResponse(exec_path)
                        geodata = subprocess.check_output(exec_path,stderr=subprocess.STDOUT,shell=True)
                        return HttpResponse(geodata)
                        if len(geodata) > 1:
                            return HttpResponse('0')
                    else:
                        return HttpResponse('invalid path')


def scan_dir(request):
    
    """ Scan project dir for files & put their geo data in the table """
    
    import glob
    import urllib
    import time
    
    out = ''
    user_path = get_path(request) + '/bcbb32527c58f770b5daef18e6a58acb/'
    os.chdir(user_path)
    file_list = glob.glob("*.*")
    for a in file_list:
        out += (str)(a) + "<br>"
        path = user_path + (str)(a)
        
        exec_path = "/root/libsgeo/test -r "+ path
        #return HttpResponse(exec_path)
        geodata = subprocess.check_output(exec_path,stderr=subprocess.STDOUT,shell=True)
        timestamp = time.time()
        
        from front_end.models import image
        
        if len(geodata) > 1:
            parse_json = json.loads(geodata)
            if parse_json:
                
                # save geodata and a filename in a table and give it unique id
                # to pull it later
                
                #timestamp = datetime.now()
                
                img = image(
                    image_filename=urllib.unquote(a),
                    image_user_id=request.user.id,
                    image_lat=parse_json['Latitude'],
                    image_lng=parse_json['Longitude'],
                    image_user_lat=parse_json['UserLatitude'],
                    image_user_lng=parse_json['UserLongitude'],
                    image_gps_lat=parse_json['GPSLatitude'],
                    image_gps_lng=parse_json['GPSLongitude'],
                    image_create_date=parse_json['DateTimeOriginal'],
                    image_project_id=request.session['data']['working_project'],
                    image_upload_date=timestamp,
                    image_json = geodata
                )
                img.save()
        else:
            # there is no geodata in the file
            img = image(
                image_filename=urllib.unquote(a),
                image_project_id=request.session['data']['working_project'],
                image_user_id=request.user.id,
                image_upload_date=timestamp
            )
            img.save()
            
    return HttpResponse(out)


def comment_save(request):
    
    """ Comment save """
    
    if request.user.is_authenticated():
        if request.method == 'POST':
            
            from front_end.models import image
            cmt = unquote(request.POST['comment'])
            file_id = request.POST['id']
            try:
                a = image.objects.get(image_user_id=request.user.id, image_id=file_id)
            except:
                return HttpResponse('not found')
            else:
                data = json.loads(a.image_json)
                data['Comments'] = cmt
                a.image_json = json.dumps(data)
                a.save()
                
            load_session(request)
            
            b = projects.objects.get(project_id=request.session['data']['working_project'])
            path = get_path(request) + '/' + b.project_hash + '/' + urllib.quote(a.image_filename)
            
            if os.path.isfile(path):
                add = ' --comments "' + request.POST['comment'] + '"'
                exec_path = settings.LIBSGEO_EXEC_PATH + "write " + add
                geodata = subprocess.check_output(exec_path,stderr=subprocess.STDOUT,shell=True)
                return HttpResponse(geodata)
            else:
                return HttpResponse('nop')


def download(request):
    
    """ File download processing """
    
    if request.user.is_authenticated():
        if request.method == 'GET':
            if request.GET.has_key('file'):
                load_session(request)
                user_root = get_path(request)

                import urllib
                
                try:
                    prj = projects.objects.get(project_id=request.session['data']['working_project'], project_user_id=request.user.id)
                except:
                    return HttpResponse(1)
                else:
                    from django.core.servers.basehttp import FileWrapper
                    
                    path = user_root + '/' + prj.project_hash + '/' + urllib.unquote(request.GET['file'])
                    
                    response = HttpResponse(FileWrapper(file(path)), content_type='image/jpeg')
                    response['Content-Disposition'] = 'attachment; filename=' + request.GET['file']
                    response['Content-Length'] = os.path.getsize(path)
                    
                    return response
                    
                    #return HttpResponse(path)

#END OF THE STORY
