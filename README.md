# geo-photo.net

(closed)

![20.jpg](https://bitbucket.org/repo/nKbgLM/images/1203569188-20.jpg)

Dependencies:

* Python 2.7

* libjpeg (http://www.ijg.org/files/)

* PIL (Python Image Library)
	python setup.py build --force
	python setup.py install

* PostgreSQL
	(OpenBSD requires sysctl increment semaphores)

* Django changed (MD5 ajax auth)
	contrib/auth/hashers.py (def make_password(_password), ...)

* PSYCOPG - PostgreSQL adapter for Python
	http://initd.org/psycopg/

* tweepy
	https://github.com/tweepy/tweepy