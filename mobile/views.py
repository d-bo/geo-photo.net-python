# -*- coding: utf-8 -*-

from django.views.decorators.csrf import csrf_exempt
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings
from django.contrib.auth.models import User

from front_end.models import image
from front_end.models import projects
from account.models import sessions

import os
import time
import json
import hashlib
import imghdr
import subprocess
from urllib import unquote


@csrf_exempt
def upload(request, string):
    
    """ Upload file """
    
    name = unquote(request.GET['name'])
    project = unquote(request.GET['prj']).encode('utf-8')
    _project_hash = hashlib.md5(project).hexdigest()
    user_hash = request.COOKIES['_trace'].encode('utf-8')
    #return HttpResponse(user_hash)
    
    """
    path = os.path.dirname(os.path.abspath(__file__)) + '/' + str(time.time()) + '_' + str(os.getpid())
    _file = open(path, 'w+')
    _file.write(str(request.FILES['file'].size))
    _file.close()
    """
    
    # check session hash
    try:
        a = sessions.objects.get(session_hash=user_hash)
    except:
        return HttpResponse(content="", status=403)
    else:
        user_id = a.session_user_id
        user_dir = user_hash
    
    # project - new / exist
    try:
        pr = projects.objects.get(project_user_id=user_id,project_hash=_project_hash)
    except:
        # create uploaded project (db)
        #prj_hash = 
        timestamp = time.time()
        pr = projects(project_name=project,project_user_id=user_id,project_hash=_project_hash,project_create_date=timestamp)
        pr.save()
    else:
        #return HttpResponse(request.COOKIES['_trace'])
        pass
    
    # project dir
    path = settings.PROJECTS_ROOT_DIR + str(user_dir) + '/' + pr.project_hash
    
    try:
        os.makedirs(path)
    except:
        pass
    else:
        pass
    
    # create file
    path = path + '/' + request.GET['name']
    _file = open(path, 'w+')
    _file.write(request.body)
    _file.close()
    
    supported_imgs = ['jpg', 'JPG', 'jpeg', 'JPEG']
    
    import imghdr
    
    # determine image header
    file_type = imghdr.what(path)
    #return HttpResponse(file_type)
    if file_type != None:
        
        #exec_path = "/home/coder/libsgeo/test -r "+ urllib2.unquote(path.replace(' ', '\ '))
        exec_path = settings.LIBSGEO_EXEC_PATH + "test -r "+ path
        #return HttpResponse(exec_path)
        geodata = subprocess.check_output(exec_path,stderr=subprocess.STDOUT,shell=True)
        
        timestamp = time.time()
        
        if len(geodata) > 1:
            parse_json = json.loads(geodata)
            if parse_json:
                img = image(
                    image_filename=unquote(request.GET['name']),
                    image_file_size=len(request.body),
                    image_user_id=user_id,
                    image_lat=parse_json['Latitude'],
                    image_lng=parse_json['Longitude'],
                    image_user_lat=parse_json['UserLatitude'],
                    image_user_lng=parse_json['UserLongitude'],
                    image_gps_lat=parse_json['GPSLatitude'],
                    image_gps_lng=parse_json['GPSLongitude'],
                    image_create_date=parse_json['DateTimeOriginal'],
                    image_project_id=pr.project_id,
                    image_upload_date=timestamp,
                    image_json = geodata
                )
                img.save()
                #return HttpResponse(parse_json['DateTimeOriginal'])
        else:
            # there is no geodata in the file
            img = image(
                image_filename=unquote(request.GET['name']),
                image_file_size=len(request.body),
                image_project_id=pr.project_id,
                image_user_id=user_id,
                image_upload_date=timestamp
            )
            img.save()
            #return HttpResponse('no geo data in file but it is saved')
        response = HttpResponse(content="", status=201)
        return response
    else:
        response = HttpResponse(content="", status=403)
        return response
    
    # put file in DB
    return HttpResponse(request.FILES['file'].size)


@csrf_exempt
def auth(request):
    
    """ Redner mobile auth template """
    
    #return HttpResponse(request.META['HTTP_USER_AGENT'])
    return render_to_response('mobile-auth.html', {}, context_instance=RequestContext(request))


def isApple(request):
    
    """ Is apple device ? """
    
    if request.META['HTTP_USER_AGENT'].find('iphone') == -1 and request.META['HTTP_USER_AGENT'].find('ipad') == -1:
        return False
    else:
        return True


def isAndroid(request):
    
    """ Is android device ? """
    
    if request.META['HTTP_USER_AGENT'].find('Android') == -1 and request.META['HTTP_USER_AGENT'].find('android') == -1:
        return False
    else:
        return True
