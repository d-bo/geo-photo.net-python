# -*- coding: utf-8 -*-

from django.conf.urls import patterns, include, url
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns('',
    
    (r'^$', 'front_end.views.index'),
    (r'^id(\d+)/$', 'front_end.views.route_id'),
    (r'^id(\d+)/(\d+)/$', 'projects.views.project_router'),
    
    #url(r'^dashboard/$', 'front_end.views.dashboard'),
    #url(r'^dashboard1/$', 'front_end.views.dashboard1'),
    url(r'^account/login/$', 'account.views.lgn'),
    url(r'^account/logout/$', 'account.views.log_out'),
    url(r'^account/register/$', 'account.views.register'),
    url(r'^account/activate/(\d+)/$', 'account.views.activate'),
    
    (r'^storage/jpeg/(\d+)/$', 'front_end.views.storage_jpeg'),
    
    # kinda api
    (r'^api/upload/$', 'front_end.views.upload'),
    (r'^api/download/$', 'front_end.views.download'),
    # current geo environment whole variables
    (r'^api/env/$', 'front_end.views.api_env'),
    
    (ur'^mobile/upload/(.*)$', 'mobile.views.upload'),
    (r'^mobile/auth/$', 'mobile.views.auth'),
    
    # working with projects
    (r'^project/create/$', 'front_end.views.project_create'),
    (r'^project/delete/$', 'front_end.views.project_delete'),
    (r'^project/load/$', 'front_end.views.project_load'),
    (r'^project/share/$', 'front_end.views.project_share'),
    (r'^project/save/$', 'front_end.views.project_save'),
    
    #
    (r'^comment/save/$', 'front_end.views.comment_save'),
    
    # filters
    (r'^filter/create/$', 'filters.views.create'),
    (r'^filter/delete/$', 'filters.views.delete'),
    (r'^filter/load/$', 'filters.views.load'),
    (r'^filter/update/$', 'filters.views.update'),
    (r'^filter/switch/$', 'filters.views.switch'),
    #(r'^filter/append'),
    
    # OAUTH
    (r'^oauth/google/$', 'oauth.views.google'),
    (r'^oauth2callback/', 'oauth.views.auth_return'),
    
    (r'^oauth/facebook/$', 'oauth_facebook.views.index'),
    (r'^oauth/facebook/callback/$', 'oauth_facebook.views.callback'),
    
    (r'^oauth/twitter/$', 'oauth_twitter.views.index'),
    (r'^oauth/twitter/callback/$', 'oauth_twitter.views.callback'),
    
    # images
    (r'^image/delete/$', 'front_end.views.image_delete'),
    (ur'^image/get/(.*)$', 'front_end.views.image_get'),
    (r'^image/thumbnails/$', 'front_end.views.image_thumbnails'),
    (r'^image/data/$', 'front_end.views.image_data'),
    (r'^image/write/data/$', 'front_end.views.image_write_data'),
    
    (r'^scan/$', 'front_end.views.scan_dir')
    
    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),
    
    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),
)
