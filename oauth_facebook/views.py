# -*- coding: utf-8 -*-

from django.http import HttpResponse
from django.shortcuts import redirect
from django.contrib.auth.models import User

import json
import urllib2
import hashlib
from urlparse import urlparse, parse_qs

from account.views import create_account
from account.views import login_user
from mobile.views import isApple
from mobile.views import isAndroid

from account.models import sessions


APP_ID = '232886440186162'
APP_SECRET = '29fc4f3a791357cb785576a4a59a147b'
CALLBACK_URL = 'http://deploy.geo-photo.net/oauth/facebook/callback/'


def index(request):
    
    """ Index redirect """
    
    return redirect('https://www.facebook.com/dialog/oauth?client_id=' + APP_ID + '&redirect_uri=' + CALLBACK_URL + '&display=touch&response_type=code')
    #return HttpResponse(json.dumps(request.GET))


def callback(request):
    
    """ Handle callback """
    
    if request.GET.has_key('code'):
        
        req = urllib2.Request('https://graph.facebook.com/oauth/access_token?client_id=' + APP_ID + '&redirect_uri=' + CALLBACK_URL + '&client_secret=' + APP_SECRET + '&code=' + request.GET['code'])
        response = urllib2.urlopen(req).read()
        
        url = str('http://no-reply.com?' + response)
        prs = parse_qs(urlparse(url).query)
        
        token = prs['access_token'][0]
        
        endpoint = urllib2.Request('https://graph.facebook.com/me?access_token=' + token)
        response = urllib2.urlopen(endpoint).read()
        
        data = json.loads(response)
        
        userdata = {}
        userdata['name'] = data['name'].encode('utf8')
        userdata['email'] = ''
        userdata['pwd'] = ''
        
        try:
            user = User.objects.get(username=userdata['name'])
        except:
            user = create_account(userdata['name'], userdata['email'])
            if user == False:
                return HttpResponse('Cannot create user. Contact site administrator.')
            namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
        else:
            namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
            try:
                ss = sessions.objects.get(session_user_id=user.id)
            except:
                out = {}
                #namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
                ss = sessions(session_user_id=user.id,session_data=out,session_hash=namehash)
                ss.save()
            else:
                pass
        
        #namehash = hashlib.md5(user.username.encode('utf8')).hexdigest()
        a = HttpResponse('<script type="text/javascript">window.opener.location.reload(false);window.close()</script>')
        
        #a = HttpResponse(namehash)
        
        if isApple(request) != False:
            request = urllib2.Request('geocam://_trace=' + str(namehash) + ';name= ' + str(userdata['name']) + ';status=200')
            return a
        if isAndroid(request) != False:
            a.set_cookie('_trace', namehash)
            a.set_cookie('status', '200')
            return a
        
        login_user(request, user)
        return a
        
    else:
        return HttpResponse('Facebook did not return code parameters. Contact site administrator.')
